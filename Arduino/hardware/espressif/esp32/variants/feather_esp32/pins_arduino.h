#ifndef Pins_Arduino_h
#define Pins_Arduino_h

#include <stdint.h>

#define EXTERNAL_NUM_INTERRUPTS 16
#define NUM_DIGITAL_PINS        40
#define NUM_ANALOG_INPUTS       16

#define analogInputToDigitalPin(p)  (((p)<20)?(esp32_adc2gpio[(p)]):-1)
#define digitalPinToInterrupt(p)    (((p)<40)?(p):-1)
#define digitalPinHasPWM(p)         (p < 34)

static const uint8_t LED_BUILTIN = 13;
#define BUILTIN_LED  LED_BUILTIN // backward compatibility

static const uint8_t TX = 17;
static const uint8_t RX = 16;

static const uint8_t SDA = 23;
static const uint8_t SCL = 22;

static const uint8_t SS    = 33;
static const uint8_t MOSI  = 18;
static const uint8_t MISO  = 19;
static const uint8_t SCK   = 5;

// mapping to match other feathers and also in order
static const uint8_t A0 = 26; 
static const uint8_t A1 = 25;
static const uint8_t A2 = 34;
static const uint8_t A3 = 39;
static const uint8_t A4 = 36;
static const uint8_t A5 = 4;
static const uint8_t A6 = 14;
static const uint8_t A7 = 32;
static const uint8_t A8 = 15;
static const uint8_t A9 = 33;
static const uint8_t A10 = 27;
static const uint8_t A11 = 12;
static const uint8_t A12 = 13;

// vbat measure
static const uint8_t A13 = 35;
//static const uint8_t Ax = 0; // not used/available
//static const uint8_t Ax = 2; // not used/available


static const uint8_t T0 = 4;
static const uint8_t T1 = 0;
static const uint8_t T2 = 2;
static const uint8_t T3 = 15;
static const uint8_t T4 = 13;
static const uint8_t T5 = 12;
static const uint8_t T6 = 14;
static const uint8_t T7 = 27;
static const uint8_t T8 = 33;
static const uint8_t T9 = 32;

static const uint8_t DAC1 = 25;
static const uint8_t DAC2 = 26;

//static const uint8_t PIN1 = GND;
//static const uint8_t PIN2 = 3.3V;
//static const uint8_t PIN3 = EN;
//static const uint8_t PIN4 = Sensor_VP;
//static const uint8_t PIN5 = Sensor_VN;
static const uint8_t PIN6 = 34;
static const uint8_t PIN7 = 35;
static const uint8_t PIN8 = 32;
static const uint8_t PIN9 = 33;
static const uint8_t PIN10 = 25;
static const uint8_t PIN11 = 26;
static const uint8_t PIN12 = 27;
static const uint8_t PIN13 = 14;
static const uint8_t PIN14 = 12;
//static const uint8_t PIN15 = GND;
static const uint8_t PIN16 = 13;
//static const uint8_t PIN17 = SD2;
//static const uint8_t PIN18 = SD3;
//static const uint8_t PIN19 = CMD;
//static const uint8_t PIN20 = CLK;
//static const uint8_t PIN21 = SD0;
//static const uint8_t PIN22 = SD1;
static const uint8_t PIN23 = 15;
static const uint8_t PIN24 = 2;
static const uint8_t PIN25 = 0;
static const uint8_t PIN26 = 4;
static const uint8_t PIN27 = 16;
static const uint8_t PIN28 = 17;
static const uint8_t PIN29 = 5;
static const uint8_t PIN30 = 18;
static const uint8_t PIN31 = 19;
//static const uint8_t PIN32 = PA31;
static const uint8_t PIN33 = 21;
//static const uint8_t PIN34 = RX0;
//static const uint8_t PIN35 = TX0;
static const uint8_t PIN36 = 22;
static const uint8_t PIN37 = 23;
//static const uint8_t PIN38 = GND;

static const uint8_t TP0 = 0;
//static const uint8_t TP1 = TXD0;
static const uint8_t TP2 = 2;
//static const uint8_t TP3 = RXD0;
static const uint8_t TP4 = 4;
static const uint8_t TP5 = 5;
//static const uint8_t TP6 = CLK;
//static const uint8_t TP7 = SD0;
//static const uint8_t TP8 = SD1;
//static const uint8_t TP9 = SD2;
//static const uint8_t TP10 = SD3;
//static const uint8_t TP11 = CMD;
static const uint8_t TP12 = 12;
static const uint8_t TP13 = 13;
static const uint8_t TP14 = 14;
static const uint8_t TP15 = 15;
static const uint8_t TP16 = 16;
static const uint8_t TP17 = 17;
static const uint8_t TP18 = 18;
static const uint8_t TP19 = 19;
//static const uint8_t TP20 = NC;
static const uint8_t TP21 = 21;
static const uint8_t TP22 = 22;
static const uint8_t TP23 = 23;
//static const uint8_t TP24 = NC;
static const uint8_t TP25 = 25;
static const uint8_t TP26 = 26;
static const uint8_t TP27 = 27;
//static const uint8_t TP28 = NC;
//static const uint8_t TP29 = NC;
//static const uint8_t TP30 = NC;
//static const uint8_t TP31 = PA31;
static const uint8_t TP32 = 32;
static const uint8_t TP33 = 33;
static const uint8_t TP34 = 34;
static const uint8_t TP35 = 35;
//static const uint8_t TP36 = Sensor_VP;
//static const uint8_t TP37 = NC;
//static const uint8_t TP38 = NC;
//static const uint8_t TP39 = Sensor_VN;
//static const uint8_t TP40 = EN;


static const uint8_t TOUCH0 = 4;
static const uint8_t TOUCH2 = 2;
static const uint8_t TOUCH3 = 15;
static const uint8_t TOUCH4 = 13;
static const uint8_t TOUCH5 = 12;
static const uint8_t TOUCH6 = 14;
static const uint8_t TOUCH7 = 27;
static const uint8_t TOUCH8 = 33;
static const uint8_t B_LEFT = 4;
static const uint8_t B_UP = 2;
static const uint8_t B_DOWN = 15;
static const uint8_t B_RIGHT = 13;
static const uint8_t B_SELECT = 12;
static const uint8_t B_START = 14;
static const uint8_t B_A = 27;
static const uint8_t B_B = 33;
static const uint8_t SCL_D = 18;
static const uint8_t SDA_D = 23;
static const uint8_t CS_D = 19;
static const uint8_t RESET_D = 25;
static const uint8_t DC_D = 26;

#endif /* Pins_Arduino_h */

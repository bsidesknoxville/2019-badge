/* 2019 Badge Example
 *  	Unless required by applicable law or agreed to in writing, this
 *	software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *    	CONDITIONS OF ANY KIND, either express or implied.
*/
#include "badgeDisplay.h"
#include "badgePad.h"
#include "app_set_name.h"
#include "badgeStore.h"

enum BUTTON_NAMES {NONE=0,LEFT=0x01,MINUS=0x02,UP=0x04,DOWN=0x08,RIGHT=0x10,SELECT=0x20,START=0x40,A=0x80,B=0x100};

#define NAME_LENGTH_LIMIT 10

static char* editfirstname = NULL;
static char* editlastname = NULL;

static int cursor_x = 0;
static int cursor_y = 0;

static const unsigned int T0_MASK = 0x0001;
static const unsigned int T1_MASK = 0x0002;
static const unsigned int T2_MASK = 0x0004;
static const unsigned int T3_MASK = 0x0008;
static const unsigned int T4_MASK = 0x0010;
static const unsigned int T5_MASK = 0x0020;
static const unsigned int T6_MASK = 0x0040;
static const unsigned int T7_MASK = 0x0080;
static const unsigned int T8_MASK = 0x0100;
static const unsigned int T9_mask = 0x0200;

static bool displayChanged = false;

static void printname(char* name, int cursor)
{
  int nameLength = strlen(name);
  Serial.printf("name length: %d\n",nameLength);
  for (int i = 0; i < nameLength && i < NAME_LENGTH_LIMIT; i++)
  {
    if (i == cursor) tft->setTextColor(ST77XX_BLACK, ST77XX_WHITE);
    tft->printf("%c",name[i]);
    if (i == cursor) tft->setTextColor(ST77XX_WHITE, ST77XX_BLACK);
  }
  tft->println("");
}

static void set_name_display()
{
  if (displayChanged)
  {
    tft->fillScreen(ST77XX_BLACK);
    tft->setTextSize(2);
    tft->setCursor(0,0);
    tft->setTextColor(ST77XX_WHITE, ST77XX_BLACK);
    if (editfirstname)
    {
      if (cursor_x == 0) printname(editfirstname, cursor_y);
      else tft->printf("%s\n",editfirstname);
    }
    if (editlastname)
    {
      if (cursor_x == 1) printname(editlastname, cursor_y);
      else tft->printf("%s\n",editlastname);
    }
    tft->println("");
    if (cursor_x == 2)
      tft->setTextColor(ST77XX_BLACK, ST77XX_WHITE);
    else
      tft->setTextColor(ST77XX_WHITE, ST77XX_BLACK);
    tft->println("Cancel");
    if (cursor_x == 3)
      tft->setTextColor(ST77XX_BLACK, ST77XX_WHITE);
    else
      tft->setTextColor(ST77XX_WHITE, ST77XX_BLACK); 
    tft->println("Save");
    displayChanged = false;
  }
}

static char fn_default[NAME_LENGTH_LIMIT+1] = "My name is";
static char ln_default[NAME_LENGTH_LIMIT+1] = "Hacker\0\0\0\0";

static void load_name()
{
  if((editfirstname = retrieveString("name_storage","firstname",NAME_LENGTH_LIMIT,fn_default))
		  == NULL) {
	  editfirstname = fn_default;
  }
  if((editlastname = retrieveString("name_storage","lastname",NAME_LENGTH_LIMIT,ln_default))
		  == NULL) {
	  editlastname = ln_default;
  }

  for (int i = 0; i < NAME_LENGTH_LIMIT; i++)
  {
    if (editfirstname[i] == '\0') editfirstname[i] = ' ';
    if (editlastname[i] == '\0') editlastname[i] = ' ';
  }
}

static void save_names()
{
  for (int i = NAME_LENGTH_LIMIT-1; i; i--)
  {
    if (editlastname[i] == ' ') editlastname[i] = '\0';
    else break;
  }
  for (int i = NAME_LENGTH_LIMIT-1; i; i--)
  {
    if (editfirstname[i] == ' ') editfirstname[i] = '\0';
    else break;
  }
  storeString("name_storage","firstname",editfirstname);
  storeString("name_storage","lastname",editlastname);
}

static void set_name_task()
{
  bool quit=false;
  load_name();
  unsigned int buttonState = 0;
  displayChanged = true;
  while(!quit)
  {    
    set_name_display();
    buttonState = readButtons();
    if (buttonState) {
      displayChanged = true;
    }
    
    if (buttonState == MINUS) {
        quit=true;
    } else if (buttonState & T0_MASK)
    {
      cursor_y -= 1;
      if (cursor_y < 0) {
        cursor_y = NAME_LENGTH_LIMIT-1;
      }
    } else if (buttonState & T2_MASK)
    {
      cursor_x -= 1;
      if (cursor_x < 0) {
        cursor_x = 3;
      }
    } else if (buttonState & T3_MASK)
    {
      cursor_x += 1;
      if (cursor_x >= 4) {
        cursor_x = 0;
      }
    } else if (buttonState & T4_MASK)
    {
      cursor_y += 1;
      if (cursor_y >= NAME_LENGTH_LIMIT) {
        cursor_y = 0;
      }
    } else if (buttonState & T7_MASK)
    {
      char tmp = 0;
      if (cursor_x == 0)
      {
        tmp = editfirstname[cursor_y];
        tmp += 1;
        if (tmp > '~') tmp = ' ';
        editfirstname[cursor_y] = tmp;
      }
      if (cursor_x == 1)
      {
        tmp = editlastname[cursor_y];
        tmp += 1;
        if (tmp > '~') tmp = ' ';
        editlastname[cursor_y] = tmp;
      }
      if (cursor_x == 2)
      {
	quit = true;
      }
      else if (cursor_x == 3)
      {
        save_names();
	tft->setTextColor(0xffff,0x0000);
	tft->printf("Saved");
    	vTaskDelay(3000 / portTICK_PERIOD_MS); //Don't check buttons too fast
      	quit = true;
      }
    } else if (buttonState & T8_MASK)
    {
      char tmp = 0;
      if (cursor_x == 0)
      {
        tmp = editfirstname[cursor_y];
        tmp -= 1;
        if (tmp < ' ') tmp = '~';
        editfirstname[cursor_y] = tmp;
      }
      if (cursor_x == 1)
      {
        tmp = editlastname[cursor_y];
        tmp -= 1;
        if (tmp < ' ') tmp = '~';
        editlastname[cursor_y] = tmp;
      }
    }
    vTaskDelay(100 / portTICK_PERIOD_MS); //Don't check buttons too fast
  }
}

void set_name_main()
{
  if (!Serial) { Serial.begin(115200); }
  while(!Serial) { vTaskDelay(100 / portTICK_PERIOD_MS); }
  Serial.println("Switching to set name");
  tft->fillScreen(ST77XX_BLACK);
  setStickyButtonsValue(false);
  setAppDelayReadValue(true);
  set_name_task();
  if(editfirstname != fn_default && editfirstname != NULL) free(editfirstname);
  if(editlastname != ln_default && editlastname != NULL) free(editlastname);
}

static const char *TAG = "set name - init";

static int x = 0;
static bool selected = false;
static bool confirmed = false;
static void updateNameInitScreen() {
	tft->fillScreen(ST77XX_BLACK);
	if (!selected) {
		tft->setCursor(5,25);
		tft->setTextSize(2);
		if (x != 0) { tft->setTextColor(0xf800,0x7ff); } 
		else { tft->setTextColor(0x7ff, 0xf800); }	
		tft->printf("Goon");
		if (x == 0) {
			tft->setTextColor(0xffff, 0x0000);
			tft->println(" <\n");
		} else { tft->printf("\n\n"); }
		if (x != 1) { tft->setTextColor(0x07e0,0xf81f); } 
		else { tft->setTextColor(0xf81f, 0x07e0); }
		tft->printf("Attendee");
		if (x == 1) {
			tft->setTextColor(0xffff, 0x0000);
			tft->println(" <\n");
		} else { tft->printf("\n\n"); }
		if (x != 2) { tft->setTextColor(0x001f,0xffe0); } 
		else { tft->setTextColor(0xffe0, 0x001f); }
		tft->printf("Speaker");
		if (x == 2) {
			tft->setTextColor(0xffff, 0x0000);
			tft->println(" <\n");
		} else { tft->printf("\n\n"); }
	} else {
		tft->setTextColor(0xffff, 0x0000);
		tft->setCursor(5,15);
		tft->println("Are\n You\n  Sure?");
		if (!confirmed) { tft->setTextColor(0x0000,0xffff); }
		else { tft->setTextColor(0xffff,0x0000); }
		tft->setCursor(20,75);
		tft->println("No");
		if (confirmed) { tft->setTextColor(0x0000,0xffff); }
		else { tft->setTextColor(0xffff,0x0000); }
		tft->setCursor(20,95);
		tft->println("Yes");
	}

}

static bool quit;

static void onLeft() { if(!selected) { if(x) { x--; updateNameInitScreen(); } }
	else { if(confirmed) { confirmed = false; updateNameInitScreen(); } } 
}
static void onUp() { onLeft(); }
static void onDown() { if (!selected ) { if(x<2) { x++; updateNameInitScreen(); } }
	else { if(!confirmed) { confirmed = true; updateNameInitScreen(); } }
}
static void onRight() { onDown(); }
static void onA() { if(selected && confirmed) {
		switch (x) {
			case 0:
				storeString("name_storage","firstname","Hi! I'm a");
				storeString("name_storage","lastname","      Goon");
				break;
			case 1:
				storeString("name_storage","firstname","I am an");
				storeString("name_storage","lastname"," Attendee");
				break;
			case 2:
				storeString("name_storage","firstname","Hey, I'm a");
				storeString("name_storage","lastname"," Speaker");
		}
		quit = true;
	}
	selected = !selected; updateNameInitScreen();
}


static void initController() {
	uint32_t buttons;
	if ((buttons = readButtons())) {
		ESP_LOGI(TAG,"Button pressed: %i",buttons);
		if (buttons & LEFT) { onLeft(); }
		if (buttons & UP) { onUp(); }
		if (buttons & DOWN) { onDown(); }
		if (buttons & RIGHT) { onRight(); }
		if (buttons & A) { onA(); }
	}
}

void set_name_init(void) {
	ESP_LOGI(TAG,"Initiating");
	updateNameInitScreen();
	setStickyButtonsValue(false);
	setAppDelayReadValue(true);
	quit = false;
	while(!quit) { initController(); vTaskDelay(100 / portTICK_PERIOD_MS); }
	ESP_LOGI(TAG,"Done");  
}

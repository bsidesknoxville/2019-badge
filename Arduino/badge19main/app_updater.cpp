/* 2019 Badge Example
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "badgePad.h"
#include "badgeDisplay.h"
#include "badgeMenu.h"
#include "app_updater.h"

enum BUTTON_NAMES {NONE=0,LEFT=0x01,MINUS=0x02,UP=0x04,DOWN=0x08,RIGHT=0x10,SELECT=0x20,START=0x40,A=0x80,B=0x100};
static const char *TAG = "updater";

static bool update;

static void updateScreen() {
	tft->fillScreen(ST77XX_BLACK);
	tft->setTextSize(3);
	tft->setCursor(2,2);
	tft->setTextColor(ST77XX_WHITE, ST77XX_BLUE);
	tft->println("UPDATE?\n");
	if (!update) {
		tft->setTextColor(ST77XX_BLACK, ST77XX_GREEN);
	} else {
		tft->setTextColor(ST77XX_GREEN, ST77XX_BLACK);
	}
	tft->print("NO");
	tft->setTextColor(ST77XX_BLACK,ST77XX_BLACK);
	tft->print("  ");
	if (update) {
		tft->setTextColor(ST77XX_BLACK, ST77XX_GREEN);
	} else {
		tft->setTextColor(ST77XX_GREEN, ST77XX_BLACK);
	}
	tft->println("YES");

}

static bool quit;

static void onLeft() { if(update) { update=false; updateScreen(); }}
static void onMinus() { quit=true; }
static void onUp() { quit=true; }
static void onDown() { quit=true; }
static void onRight() { if(!update) { update=true; updateScreen(); } }
static void onSelect() { quit=true; }
static void onStart() { quit=true; }
static void onA() { if(!update) { quit=true; return; }
	tft->fillScreen(ST77XX_BLACK);
	tft->setTextSize(3);
	tft->setCursor(0,0);
	tft->setTextColor(ST77XX_RED, ST77XX_BLACK);
	tft->println("UpdateN\n");
	vTaskDelete(badgeSleepTaskHandler);
	update_badge19();
	// The update failed. sry.
	tft->setTextColor(ST77XX_RED, ST77XX_BLACK);
	tft->setCursor(5,tft->getCursorY());
	tft->println("FAILED");
	vTaskDelay(10000 / portTICK_PERIOD_MS);
	quit = true;
	esp_restart(); // we can't try again until we do anyways,
	// it'll crash if we try again
}
static void onB() { quit=true; }


static void controller() {
	uint32_t buttons;
	if ((buttons = readButtons())) {
		if (buttons == 0xffff) { updateScreen(); } // Back from light sleep mode
		else {
			if (buttons & LEFT) { onLeft(); }
			if (buttons & MINUS) { onMinus(); }
			if (buttons & UP) { onUp(); }
			if (buttons & DOWN) { onDown(); }
			if (buttons & RIGHT) { onRight(); }
			if (buttons & SELECT) { onSelect(); }
			if (buttons & START) { onStart(); }
			if (buttons & A) { onA(); }
			if (buttons & B) { onB(); }
			if ((buttons & (SELECT | START)) == (SELECT | START)) { quit = true; }
		}
	}
}

void app_updater_main(void) {
	ESP_LOGI(TAG,"Initiating");
	update=false;
	updateScreen();
	setStickyButtonsValue(false);
	setAppDelayReadValue(true);
	quit = false;
	vTaskDelay(1000 / portTICK_PERIOD_MS);
	while(!quit) { controller(); vTaskDelay(100 / portTICK_PERIOD_MS); }
	ESP_LOGI(TAG,"Quitting");  
}

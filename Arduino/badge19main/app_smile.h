/* 2019 Badge Example
 *    Unless required by applicable law or agreed to in writing, this
 *       software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *          CONDITIONS OF ANY KIND, either express or implied.
 *          */

#ifndef APP_SMILE
#define APP_SMILE
#if defined (__cplusplus)
extern "C" {
#endif
void smile_main();
#if defined (__cplusplus)
}
#endif
#endif

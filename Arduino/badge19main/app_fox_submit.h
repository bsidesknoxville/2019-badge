/* 2019 Badge Example
 *    Unless required by applicable law or agreed to in writing, this
 *    software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *    CONDITIONS OF ANY KIND, either express or implied.
*/
#ifndef FOX_SUBMIT
#define FOX_SUBMIT
#if defined (__cplusplus)
extern "C" {
#endif
void fox_submit_main();
void fox_submit_init();
#if defined (__cplusplus)
}
#endif
#endif

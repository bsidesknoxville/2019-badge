/* 2019 Badge Example
 *    Unless required by applicable law or agreed to in writing, this
 *       software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *          CONDITIONS OF ANY KIND, either express or implied.
 *          */

#ifndef BADGE_WIFI
#define BADGE_WIFI
#define CONFIG_FIRMWARE_UPGRADE_URL "https://update.zoo:8070/latest.bin"
#define CONFIG_WIFI_SSID "BS2019-Badge"
#define CONFIG_WIFI_PASSWORD "KJ9or26QxRvOto1"
#if defined (__cplusplus)
extern "C" {
#endif
#include "esp_event_loop.h"
extern const int CONNECTED_BIT;
void initialise_wifi(void);

extern EventGroupHandle_t wifi_event_group;
extern const char * ca_cert_pem;
#if defined (__cplusplus)
}
#endif
#endif

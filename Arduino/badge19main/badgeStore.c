/* 2019 Badge Example
 *  	Unless required by applicable law or agreed to in writing, this
 *	software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *    	CONDITIONS OF ANY KIND, either express or implied.
*/
#include "nvs_flash.h"
#include "nvs.h"
#include "esp_log.h"
#include "badgeStore.h"
#include "app_set_name.h"
#include <stdlib.h>

static const char *TAG = "badge19storage";

void badgeStoreInit() {
	esp_err_t err = nvs_flash_init();
	if (err == ESP_ERR_NVS_NO_FREE_PAGES) {
		ESP_ERROR_CHECK(nvs_flash_erase());
		nvs_flash_init();
	}

	char *name;
	if ((name = retrieveString("name_storage","firstname",10,NULL)) == NULL) {
		set_name_init();
	}
	free(name);
}


char *retrieveString(const char *store,
		const char *str,
		const int max_size,
		const char *default_str) {
	 esp_err_t err;
	 size_t strlength;    
	 char *ptr;
	 nvs_handle read_store;
	 nvs_open(store, NVS_READWRITE, &read_store);        
	 if ((err = nvs_get_str(read_store, str, NULL, &strlength)) == ESP_ERR_NVS_NOT_FOUND)
	 {
		 if (default_str == NULL) {
			 ESP_LOGI(TAG,"%s not found - and no default set",store);
			 return NULL;
		 }	       
		 ESP_LOGI(TAG,"%s not found - Storing default %s",store,default_str); 
		 nvs_set_str(read_store, str, default_str);
		 if ((err = nvs_get_str(read_store, str, NULL, &strlength)) == ESP_ERR_NVS_NOT_FOUND) {
			 ESP_LOGI(TAG,"%s not found - Returning null",store);
			 nvs_close(read_store);
			 return NULL;
		 } else if (err != ESP_OK) { 
			 ESP_LOGI(TAG,"general storage error"); 
			 nvs_close(read_store);
			 return NULL;
		 }
	 } else if (err != ESP_OK) { 
		ESP_LOGI(TAG,"general storage error");
		nvs_close(read_store);
		return NULL; 
	 }
	 ESP_LOGI(TAG,"loading from storage");
	 if (strlength > max_size) { strlength = max_size; }
	 strlength+=1;
	 ptr = (char *) calloc(max_size+1,1);
	 if (ptr == NULL) { 
		 ESP_LOGI(TAG,"malloc failed during nvs retrival");
		 nvs_close(read_store);
		 free(ptr);
		 return NULL; 
	 };
	 if ((err = nvs_get_str(read_store, str, ptr, &strlength)) == ESP_ERR_NVS_NOT_FOUND) {
		 ESP_LOGI(TAG,"general storage error");
		 nvs_close(read_store);
		 free(ptr);
		 return NULL; 
	 } else if (err != ESP_OK) {
		 ESP_LOGI(TAG,"general storage error");
		 nvs_close(read_store);
		 free(ptr);
		 return NULL;
	 }
	 nvs_close(read_store);
	 return ptr;
}

void storeString(const char *store,
		const char *str,
		char *val) {
	 nvs_handle wr_store;
	 nvs_open(store, NVS_READWRITE, &wr_store);
	 nvs_set_str(wr_store, str, val);
	 nvs_close(wr_store);
}

/* 2019 Badge
 *    Unless required by applicable law or agreed to in writing, this
 *    software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *    CONDITIONS OF ANY KIND, either express or implied.
 */
//  Jed's GIF  example

#include <SPI.h>
#include <TFT_eSPI.h>
// Hardware-specific library, be sure to have correctly edited /Users/XXXX/Documents/Arduino/libraries/TFT_eSPI/User_Setup.h 
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "badgeDisplay.h"
#include "badgePad.h"
#include "app_gif.h"
#include "app_gif_mario.h"

void gif_main()
{
	int delayTime = 80;
	TFT_eSPI tft = TFT_eSPI();  // Invoke custom library
  	tft.init();
  	tft.fillScreen(TFT_BLACK);
  	tft.setAddrWindow(0,0,128,128);
  	tft.setSwapBytes(1);// Swap the byte order for pushImage() - corrects endianness
  
	int x;
	for(x = 0; x < num_frames*3; x++){
		Serial.print("Printing Frame # ");
		Serial.println(x);
    		tft.pushImage(0,0,128,128,img_pointer[x%num_frames]);
    		vTaskDelay(delayTime/portTICK_PERIOD_MS);
		if(((x+1) % num_frames) == 0) {
    			vTaskDelay(500/portTICK_PERIOD_MS);
		}
	}
}


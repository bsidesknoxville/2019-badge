/* 2019 Badge Example
 *    Unless required by applicable law or agreed to in writing, this
 *    software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *    CONDITIONS OF ANY KIND, either express or implied.
 */

#ifndef NAME_AND_TIME
#define NAME_AND_TIME
#if defined (__cplusplus)
extern "C" {
#endif
void name_time_main();
#if defined (__cplusplus)
}
#endif
#endif


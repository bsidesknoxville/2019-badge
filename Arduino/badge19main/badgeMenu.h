/* 2019 Badge Example
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#ifndef BADGEMENU
#define BADGEMENU
#if defined (__cplusplus)
extern "C" {
#endif
void badgeMainTask(void *pvParamter);
extern TaskHandle_t badgeMainTaskHandler;
extern TaskHandle_t badgeSleepTaskHandler;
extern bool quitMainTask;
extern bool quitSleepTask;
#if defined (__cplusplus)
}
#endif
#endif


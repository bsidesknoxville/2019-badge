/* 2019 Badge Example
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_task_wdt.h"
#include "badgePad.h"
#include "badgeDisplay.h"
#include "badgeMenu.h"
#include "badgeStore.h"
#include "esp_log.h"

extern "C" {
	TaskHandle_t badgeMainTaskHandler = NULL;
	TaskHandle_t badgeSleepTaskHandler = NULL;
}

#ifdef CONFIG_AUTOSTART_ARDUINO
	#warning "Enabling Arduino setup and loop"
	static void app_arduino();
	/* Arduino Setup */
	extern TaskHandle_t loopTaskHandle;
	void setup() {
		app_arduino();
		esp_task_wdt_delete(loopTaskHandle);
		vTaskDelete(loopTaskHandle);
	}
	void loop() {}
	static void app_arduino()
#else
	extern "C" { void app_main(); }
	void app_main()
#endif
{
    ESP_LOGI("main","Initiating Pad\n");
    badgePadInit();
    ESP_LOGI("main","Initiating Display\n");
    badgeDisplayInit();
    ESP_LOGI("main","Initiating Store\n");
    badgeStoreInit();
    ESP_LOGI("main","Starting Tasks");
    xTaskCreate(&badgeMainTask, "main task", 32768, NULL, 1, &badgeMainTaskHandler);
    xTaskCreate(&badgeSleepTask, "sleep task", 2048, NULL, 1, &badgeSleepTaskHandler);
}

/* 2019 Badge
 *    Unless required by applicable law or agreed to in writing, this
 *    software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *    CONDITIONS OF ANY KIND, either express or implied.
 */
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "badgeDisplay.h"
#include "badgePad.h"
#include "app_smile.h"

static int counter = 0;

static int x,y;
static void smile_display1()
{
  tft->fillScreen(ST77XX_BLACK);
  tft->setCursor(0,0);
  tft->setTextColor(ST77XX_WHITE, ST77XX_BLACK);
  tft->setTextSize(3);
  tft->println("Smile\ntest:\n");
  x = tft->getCursorX();
  y = tft->getCursorY();
}

static void smile_display2()
{
  tft->setCursor(x,y);
  tft->printf("0x%08x",counter);

}

static void smile_task()
{
  smile_display1();
  while (!(readButtons()&0x02))
  {
    //Serial.println("In smile task");
    counter++;
    smile_display2();
    vTaskDelay(100/portTICK_PERIOD_MS);
  }
}

void smile_main()
{
  Serial.println("Switching to smile");
  tft->fillScreen(ST77XX_BLACK);
  smile_task();
}

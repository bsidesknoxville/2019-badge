/* 2019 Badge Example
 *    Unless required by applicable law or agreed to in writing, this
 *    software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *    CONDITIONS OF ANY KIND, either express or implied.
 */


#include <HTTPClient.h>
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"

#include "time.h"

#include "badgeDisplay.h"
#include "badgePad.h"
#include "badgeStore.h"
#include "app_name_and_time.h"
#include "badgeWifi.h"

enum BUTTON_NAMES {NONE=0,LEFT=0x01,MINUS=0x02,UP=0x04,DOWN=0x08,RIGHT=0x10,SELECT=0x20,START=0x40,A=0x80,B=0x100};

static const char* ntpServer = "ntp.zoo";
static const long  gmtOffset_sec = 3600;
static const int   daylightOffset_sec = 3600;

static struct tm timeinfo;
static char* firstname = NULL;
static char* lastname = NULL;

static void name_time_display()
{
  tft->fillScreen(ST77XX_BLACK);
  tft->setCursor(0,0);
  tft->setTextColor(ST77XX_WHITE, ST77XX_BLACK);
  tft->setTextSize(3);
  tft->printf("%s\n",firstname);
  tft->setTextSize(2);
  tft->printf("%s\n",lastname);
  tft->println("");
  tft->setTextSize(1);
  tft->println(&timeinfo, "%B %d %Y");
  tft->println(&timeinfo, "%H:%M:%S");
}

#define NAME_LENGTH_LIMIT 10
static char fn_default[NAME_LENGTH_LIMIT+1] = "My name is";
static char ln_default[NAME_LENGTH_LIMIT+1] = "Hacker\0\0\0\0";

static void load_name()
{
	if((firstname = retrieveString("name_storage","firstname",NAME_LENGTH_LIMIT,fn_default)) == NULL) {
		firstname = fn_default;
	}
	if((lastname = retrieveString("name_storage","lastname",NAME_LENGTH_LIMIT,ln_default)) == NULL) {
		lastname = ln_default;
	}
}

static void name_time_task()
{
  unsigned int buttonState = 0;
  bool quit=false;
  load_name();

  initialise_wifi();
  /* Wait for the callback to set the CONNECTED_BIT in the
   *      event group.
   *        */
  xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
		          false, true, portMAX_DELAY);

  Serial.println(" CONNECTED");
  
  //init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);

  while (!quit)
  {
    getLocalTime(&timeinfo);
    name_time_display();
    vTaskDelay(100/portTICK_PERIOD_MS);
    buttonState = readButtons();
    if (buttonState == MINUS) {
        quit=true;
    }
  }
}

void name_time_main()
{
  int x = tft->getRotation();
  Serial.println("Switching to name and time");
  tft->setRotation(3);
  tft->fillScreen(ST77XX_BLACK);
  tft->setCursor(0,0);
  tft->setTextColor(ST77XX_WHITE, ST77XX_BLACK);
  tft->setTextSize(2);
  tft->println("Connecting");
  tft->println("To NTP");
  name_time_task();
  tft->setRotation(x);
}

/* 2019 Badge Example
 *    Unless required by applicable law or agreed to in writing, this
 *       software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *          CONDITIONS OF ANY KIND, either express or implied.
 *          */

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"

#include "nvs.h"
#include "nvs_flash.h"

#include "badgeWifi.h"

static const char *TAG = "badge19main_updater";

/* FreeRTOS event group to signal when we are connected & ready to make a request */

/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */

esp_err_t _http_event_handler(esp_http_client_event_t *evt)
{
    switch(evt->event_id) {
        case HTTP_EVENT_ERROR:
            ESP_LOGD(TAG, "HTTP_EVENT_ERROR");
            break;
        case HTTP_EVENT_ON_CONNECTED:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_CONNECTED");
            break;
        case HTTP_EVENT_HEADER_SENT:
            ESP_LOGD(TAG, "HTTP_EVENT_HEADER_SENT");
            break;
        case HTTP_EVENT_ON_HEADER:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_HEADER, key=%s, value=%s", evt->header_key, evt->header_value);
            break;
        case HTTP_EVENT_ON_DATA:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_DATA, len=%d", evt->data_len);
            break;
        case HTTP_EVENT_ON_FINISH:
            ESP_LOGD(TAG, "HTTP_EVENT_ON_FINISH");
            break;
        case HTTP_EVENT_DISCONNECTED:
            ESP_LOGD(TAG, "HTTP_EVENT_DISCONNECTED");
            break;
    }
    return ESP_OK;
}

void update_badge19(void) {
	initialise_wifi();
	ESP_LOGI(TAG, "Waiting for wifi to connect...");

	/* Wait for the callback to set the CONNECTED_BIT in the
	   event group.
	*/
	xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
			    false, true, portMAX_DELAY);
	ESP_LOGI(TAG, "Connected to WiFi network! Attempting to connect to server...");

	esp_http_client_config_t config = {
	    .url = CONFIG_FIRMWARE_UPGRADE_URL,
	    .cert_pem = (char *)ca_cert_pem,
	    .event_handler = _http_event_handler,
	};
	esp_err_t ret = esp_https_ota(&config);
	if (ret == ESP_OK) {
	    esp_restart();
	} else {
	    ESP_LOGE(TAG, "Firmware upgrade failed");
	}
}

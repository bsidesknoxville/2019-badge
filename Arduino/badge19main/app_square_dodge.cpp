/* 2019 Badge Example
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "badgePad.h"
#include "badgeDisplay.h"
#include "badgeMenu.h"
#include "app_square_dodge.h"
#include "esp_system.h"
#include "nvs_flash.h"
#include "nvs.h"

static bool game_pause;
static bool game_quit;
static uint16_t game_tp[TOUCH_PAD_MAX];

class SquareDodge {
public:
  typedef struct config {
    static const unsigned short FPS = 75;
    static const unsigned short TARGET_FRAME_TIME = 1000./FPS;
    static const unsigned short W = 128;
    static const unsigned short H = 128;
    static const unsigned short MAX_BULLETS = 64;
    static const unsigned short MAX_ANIMATIONS = 8;
    static const unsigned short MAX_EMITTERS = 1;
    static const unsigned short BOSS_SIZE = 4;
    static const unsigned short BOSS_HITBOX = 8;
    static const unsigned short PLAYER_SIZE = 8;
    static const unsigned short PLAYER_HITBOX = 4;
    static const unsigned short BG_COLOR = 0x0000;
    static constexpr signed short PLAYER_BOUNDARY[4] = {4, 4, 124, 124}; // x1, x2, y1, y2
    static const unsigned short PLAYER_MOVEMENT_MULTIPLIER = 2;
    static const unsigned int NUMBER_OF_LEVELS = 4;
  } config;

  // r, g, b should be values from 0-255 (24-bit color), it will be converted to 16-bit
  static unsigned short rgb2hex24(unsigned char r, unsigned char g, unsigned char b) {
    return (((unsigned char)(r/256.*32) << 11) + ((unsigned char)(g/256.*64) << 5) + ((unsigned char)(b/256.*32)));
  }

  // r is 0-31, g is 0-63, b is 0-31
  static unsigned short rgb2hex16(unsigned char r, unsigned char g, unsigned char b) {
    return ((r << 11) + (g << 5) + b);
  }

  static unsigned short randomlight() {
    int r = random(0, 3);
    if(r == 0) return rgb2hex16(31, random(0, 63), random(0, 31));
    else if(r == 1) return rgb2hex16(random(0, 31), random(0, 63), 31);
    else return rgb2hex16(random(0, 31), 63, random(0, 31));
  }

  // PLAYER

  typedef struct player {
    short x;
    short y;
    unsigned short oc = ST77XX_WHITE; // outer color
    unsigned short ic = ST77XX_RED; // inner color
  } player;

  void player_movement() {
    if(game_tp[0] < 384 || game_tp[2] < 384 || game_tp[3] < 384 || game_tp[4] < 384 ) {
      s.rng_movement = false;
    }
    if(s.rng_movement) {
      if(random(10) == 0) s.rng_x = random(-1, 2);
      if(random(10) == 0) s.rng_y = random(-1, 2);
      s.p.x += s.rng_x;
      s.p.y += s.rng_y;
    } else {
      if(!(game_tp[0] < 384 && game_tp[4] < 384))  s.p.x += (game_tp[0] < 384 ? -1 : 0 + game_tp[4] < 384 ? 1 : 0);
      if(!(game_tp[2] < 384 && game_tp[3] < 384)) s.p.y += (game_tp[2] < 384 ? -1 : 0 + game_tp[3] < 384 ? 1 : 0);
    }
    if(s.p.x < config::PLAYER_BOUNDARY[0]) s.p.x = config::PLAYER_BOUNDARY[0];
    if(s.p.y < config::PLAYER_BOUNDARY[1]) s.p.y = config::PLAYER_BOUNDARY[1];
    if(s.p.x > config::PLAYER_BOUNDARY[2]) s.p.x = config::PLAYER_BOUNDARY[2];
    if(s.p.y > config::PLAYER_BOUNDARY[3]) s.p.y = config::PLAYER_BOUNDARY[3];
  }

  void player_tick(player *p) {
    tft->fillRect((short)(p->x - config::PLAYER_SIZE/2.), (short)(p->y - config::PLAYER_SIZE/2.), config::PLAYER_SIZE, config::PLAYER_SIZE, config::BG_COLOR); // undraw
    player_movement();
    tft->fillRect((short)(p->x - config::PLAYER_SIZE/2.), (short)(p->y - config::PLAYER_SIZE/2.), config::PLAYER_SIZE, config::PLAYER_SIZE, p->oc); // draw outer box
    tft->fillRect((short)(p->x - config::PLAYER_HITBOX/2.), (short)(p->y - config::PLAYER_HITBOX/2.), config::PLAYER_HITBOX, config::PLAYER_HITBOX, p->ic); // draw inner box
  }

  // BOSS

  typedef struct boss {
    short x;
    short y;
    unsigned short sc = ST77XX_YELLOW; // solid color
    unsigned short oc = ST77XX_YELLOW; // outline color
  } boss;

  void boss_tick(boss *b) {
    tft->fillRect((short)(b->x - config::BOSS_SIZE/2.), (short)(b->y - config::BOSS_SIZE/2.), config::BOSS_SIZE, config::BOSS_SIZE, config::BG_COLOR); // undraw
    tft->drawRect((short)(b->x - config::BOSS_HITBOX/2.), (short)(b->y - config::BOSS_HITBOX/2.), config::BOSS_HITBOX, config::BOSS_HITBOX, config::BG_COLOR); // undraw

    tft->fillRect((short)(b->x - config::BOSS_SIZE/2.), (short)(b->y - config::BOSS_SIZE/2.), config::BOSS_SIZE, config::BOSS_SIZE, b->sc); // draw solid box
    tft->drawRect((short)(b->x - config::BOSS_HITBOX/2.), (short)(b->y - config::BOSS_HITBOX/2.), config::BOSS_HITBOX, config::BOSS_HITBOX, b->oc); // draw outline
  }

  // BULLET

  typedef struct bullet {
    float x;
    float y;
    float vx; // pixels per frame in x
    float vy; // pixels per frame in y
    short s; // size
    short c; // 16-bit color
    bool active = true;
  } bullet;

  bool bullet_tick(bullet *b) {
    tft->fillRect((short)(b->x - b->s/2.), (short)(b->y - b->s/2.), b->s, b->s, config::BG_COLOR);
    if(!b->active) return false;
    float fx = b->x + b->vx;
    float fy = b->y + b->vy;
    b->x = fx;
    b->y = fy;

    if(b->x > 0 && b->y > 0 && b->x < config::W && b->y < config::H) {
      tft->fillRect((short)(b->x - b->s/2.), (short)(b->y - b->s/2.), b->s, b->s, b->c);
    } else {
      return false;
    }
    return true;
  }

  void bullet_tick_all() {
    for(unsigned i = 0; i < config::MAX_BULLETS; i++) {
      if(s.obj[i] != NULL) {
        if(!bullet_tick(s.obj[i])) {
          free(s.obj[i]);
          s.obj[i] = NULL;
        }
      }
    }
  }

  bool bullet_add(bullet b) {
    for(unsigned i = 0; i < config::MAX_BULLETS; i++) {
      if(s.obj[i] == NULL) {
        s.obj[i] = (bullet *)malloc(sizeof(bullet));
        memcpy(s.obj[i], &b, sizeof(bullet));
        s.score++;
        return true;
      }
    }
    return false;
  }

  // ANIMATION

  typedef struct animation {
    short x;
    short y;
    unsigned long start; // start frame
    unsigned long frames; // number of frames
    SquareDodge *game; // pointer to something to be used by tick
    void (*tick)(animation *);
  } animation;

  void animation_tick_all() {
    for(unsigned i = 0; i < config::MAX_ANIMATIONS; i++) {
      if(s.ani[i] != NULL) {
        (*s.ani[i]->tick)(s.ani[i]);
        if(s.frame >= s.ani[i]->start && s.ani[i]->frames > 0) {
          s.ani[i]->frames--;
        } else {
          free(s.ani[i]);
          s.ani[i] = NULL;
        }
      }
    }
  }

  bool animation_add(animation a) {
    for(unsigned i = 0; i < config::MAX_ANIMATIONS; i++) {
      if(s.ani[i] == NULL) {
        s.ani[i] = (animation *)malloc(sizeof(animation));
        memcpy(s.ani[i], &a, sizeof(animation));
        return true;
      }
    }
    return false;
  }

  static void animation_player_implode(animation *a) {
    if(a->game->s.frame == a->start) {
      a->game->s.p.oc = ST77XX_ORANGE;
    }
    a->game->tft->drawRect(a->x - (a->frames/2), a->y - (a->frames/2), a->frames, a->frames, config::BG_COLOR);
    if(a->frames > 0) {
      a->game->tft->drawRect(a->x - (a->frames - 1)/2, a->y - (a->frames - 1)/2, a->frames - 1, a->frames - 1, ST77XX_ORANGE);
    } else {
      a->game->s.p.oc = ST77XX_WHITE;
    }
  }

  // EMITTER

  typedef struct emitter {
    short x;
    short y;
    unsigned long frames;
    void (*tick)(emitter *);
    SquareDodge *game;
  } emitter;

  static void emitter_random_tick(emitter *e) {
    if(e->frames%2 == 0) {
      bullet b;
      b.x = e->game->s.b.x;
      b.y = e->game->s.b.y;
      float theta = random(0, 361) * 3.1415926535/180;
      b.vx = random(50, 150)/100.*cos(theta);
      b.vy = random(50, 150)/100.*sin(theta);
      b.s = random(2, 6);
      b.c = randomlight();
      e->game->bullet_add(b);
    }
  }

  static void emitter_spiral_tick(emitter *e) {
    const unsigned int SPEED = 10;
    if(e->frames%2 == 0) {
      bullet b;
      b.x = e->game->s.b.x;
      b.y = e->game->s.b.y;
      float theta = ((e->game->s.frame % 36)*SPEED)*3.1415962535/180;
      b.vx = cos(theta);
      b.vy = sin(theta);
      b.s = 4;
      b.c = randomlight();
      e->game->bullet_add(b);
    }
  }

  static void emitter_pinwheel_tick(emitter *e) {
    const unsigned int SPEED = 10;
    const unsigned int SPOKES = 4;
    if(e->frames%5 == 0) {
      for(unsigned i = 0; i < SPOKES; i++) {
        bullet b;
        b.x = e->game->s.b.x;
        b.y = e->game->s.b.y;
        float theta = ((e->game->s.frame*SPEED + i*(360/SPOKES))%360)*3.1415962535/180;
        b.vx = cos(theta);
        b.vy = sin(theta);
        b.s = 4;
        b.c = randomlight();
        e->game->bullet_add(b);

      }
    }
  }

  static void emitter_targeted_burst(emitter *e) {
    if(e->frames%40 == 0) {
      for(unsigned i = 0; i < 8; i++) {
        float theta = atan2((e->game->s.p.y + random(-24, 25)) - e->game->s.b.y, (e->game->s.p.x + random(-24, 25)) - e->game->s.b.x);
        bullet b;
        b.x = e->game->s.b.x;
        b.y = e->game->s.b.y;
        b.vx = cos(theta)*random(50, 100)/100.;
        b.vy = sin(theta)*random(50, 100)/100.;
        b.s = random(2, 6);
        b.c = randomlight();
        e->game->bullet_add(b);
      }
    }
  }

  static void emitter_targeted_single(emitter *e) {
    if(e->frames%20 == 0) {
        float theta = atan2(e->game->s.p.y - e->game->s.b.y, e->game->s.p.x - e->game->s.b.x);
        bullet b;
        b.x = e->game->s.b.x;
        b.y = e->game->s.b.y;
        b.vx = cos(theta)*random(50, 100)/100.;
        b.vy = sin(theta)*random(50, 100)/100.;
        b.s = 4;
        b.c = randomlight();
        e->game->bullet_add(b);
    }
  }

  static void emitter_empty_tick(emitter *e) {
    // intentionally left blank
  }

  void emitter_tick_all() {
    for(unsigned i = 0; i < config::MAX_EMITTERS; i++) {
      if(s.emi[i] != NULL) {
        (*s.emi[i]->tick)(s.emi[i]);
        if(s.emi[i]->frames != 0) { // set to -1 for infinite
          s.emi[i]->frames--;
        } else {
          free(s.emi[i]);
          s.emi[i] = NULL;
        }
      }
    }
  }

  bool emitter_add(emitter e) {
    for(unsigned i = 0; i < config::MAX_EMITTERS; i++) {
      if(s.emi[i] == NULL) {
        s.emi[i] = (emitter *)malloc(sizeof(emitter));
        memcpy(s.emi[i], &e, sizeof(emitter));
        return true;
      }
    }
    return false;
  }

  // COLLISION

  bool collision_detect() {
    for(unsigned i = 0; i < config::MAX_BULLETS; i++) {
      if(s.obj[i] != NULL) {
        // https://developer.mozilla.org/en-US/docs/Games/Techniques/2D_collision_detection
        bool collide = false;

        typedef struct rect {
          short x;
          short y;
          short width;
          short height;
        } rect;

        bullet *b = s.obj[i];
        rect rect1 = {(short)(b->x - b->s/2.), (short)(b->y - b->s/2.), b->s, b->s};

        player *p = &s.p;
        rect rect2 = {(short)(p->x - config::PLAYER_HITBOX/2.), (short)(p->y - config::PLAYER_HITBOX/2.), config::PLAYER_HITBOX, config::PLAYER_HITBOX};

        if (rect1.x < rect2.x + rect2.width &&
            rect1.x + rect1.width > rect2.x &&
            rect1.y < rect2.y + rect2.height &&
            rect1.y + rect1.height > rect2.y) {
          collide = true;
        }

        if(collide) {
          s.obj[i]->active = false;
          animation a;
          a.x = (short)s.p.x;
          a.y = (short)s.p.y;
          a.start = s.frame;
          a.frames = 30;
          a.game = this;
          a.tick = animation_player_implode;
          animation_add(a);
          // Scores will only be recorded on collision or a clean exit
          if(s.score > s.highscore) {
            s.highscore = s.score;
            nvs_set_u64(s.nvsh, "high_score", s.highscore);
            nvs_commit(s.nvsh);
          }
          s.score = 0;
          return true;
        }
      }
    }
    return false;
  }

  // LEVEL

  emitter level[config::NUMBER_OF_LEVELS];
  void initialize_levels() {
    for(unsigned i = 0; i < config::NUMBER_OF_LEVELS; i++) {
      level[i].x = s.b.x;
      level[i].x = s.b.y;
      level[i].frames = 600;
      level[i].tick = emitter_empty_tick;
      level[i].game = this;
    }
    level[0].tick = emitter_spiral_tick;
    level[1].tick = emitter_targeted_burst;
    level[2].tick = emitter_pinwheel_tick;
    level[3].tick = emitter_random_tick;
  }

  // STATE

  typedef struct state {
    unsigned long frame = 0; // frame counter
    unsigned int level = 0;
    bool rng_movement = false; // set to true to have the player move around randomly
    player p;
    int rng_x = 0;
    int rng_y = 0;
    boss b;
    bullet *obj[config::MAX_BULLETS]; // bullet list
    animation *ani[config::MAX_ANIMATIONS]; // animation list
    emitter *emi[config::MAX_EMITTERS]; // emitter list
    uint64_t score = 0;
    uint64_t highscore = 0;
    nvs_handle nvsh;
  } state;

  Adafruit_ST7735 *tft;
  state s;

  void score_screen() {
    // save the score at this point just in case
    if(s.score > s.highscore) {
      s.highscore = s.score;
      nvs_set_u64(s.nvsh, "high_score", s.highscore);
      nvs_commit(s.nvsh);
    }
    short add = -4;
    unsigned short title_color = 252;
    tft->fillScreen(ST77XX_BLACK);
    tft->setTextSize(2);
    tft->setCursor(0, 4);
    tft->setTextColor(rgb2hex24(255, title_color, 0));
    tft->printf("PAUSED\n");
    tft->setTextSize(1);
    tft->printf("\n");
    tft->setTextSize(2);
    tft->setTextColor(0xfcc0);
    tft->printf("Score\n");
    tft->setTextColor(0xffff);
    tft->printf("%llu\n", s.score);
    tft->setTextColor(0xfcc0);
    tft->printf("High Score\n");
    tft->setTextColor(0xffff);
    tft->printf("%llu\n", s.highscore);
    tft->setTextSize(1);
    tft->printf("\nPress S7 to resume");
    unsigned long c = 0;
    tft->setTextSize(2);
    while(1) {
      if(game_tp[7] < 384) {
        c++;
      } else {
        c = 0;
      }
      if(c > 4) {
        tft->fillScreen(ST77XX_BLACK);
        game_pause = false;
        break;
      }
      tft->setCursor(0, 4);
      tft->setTextColor(rgb2hex24(252, 252-title_color, title_color));
      tft->printf("PAUSED");
      title_color += add;
      if(title_color >= 252) add = -4;
      if(title_color <= 32) add = 4;
      vTaskDelay(10/portTICK_PERIOD_MS);
    }
  }

  SquareDodge(Adafruit_ST7735 *tft): tft(tft) { 
    esp_err_t err = nvs_open("SquareDodge", NVS_READWRITE, &s.nvsh);
    if (err == ESP_OK) {
      nvs_get_u64(s.nvsh, "high_score", &s.highscore);
    }

    s.p.x = 64;
    s.p.y = 96;
    s.b.x = 64;
    s.b.y = 32;

    for(unsigned i = 0; i < config::MAX_BULLETS; i++) {
      s.obj[i] = NULL;
    }
    for(unsigned i = 0; i < config::MAX_ANIMATIONS; i++) {
      s.ani[i] = NULL;
    }
    for(unsigned i = 0; i < config::MAX_EMITTERS; i++) {
      s.emi[i] = NULL;
    }

    initialize_levels();
    tft->fillScreen(config::BG_COLOR);
  }

  ~SquareDodge() {
    if(s.score > s.highscore) {
      s.highscore = s.score;
      nvs_set_u64(s.nvsh, "high_score", s.highscore);
      nvs_commit(s.nvsh);
    }
    nvs_close(s.nvsh);
    for(unsigned i = 0; i < config::MAX_BULLETS; i++) {
      if(s.obj[i] != NULL) {
        free(s.obj[i]);
        s.obj[i] = NULL;
      }
    }
    for(unsigned i = 0; i < config::MAX_ANIMATIONS; i++) {
      if(s.ani[i] != NULL) {
        free(s.ani[i]);
        s.ani[i] = NULL;
      }
    }
  }


  void loop() {
    if(emitter_add(level[s.level])) {
      s.level = (s.level + 1)%config::NUMBER_OF_LEVELS;
    }
    unsigned long t1 = esp_timer_get_time(); // microseconds

    emitter_tick_all();
    boss_tick(&s.b);
    player_tick(&s.p);
    bullet_tick_all();
    animation_tick_all();
    collision_detect();

    unsigned long t2 = esp_timer_get_time();
    unsigned long tdelta = (t2 - t1);
    unsigned long target = config::TARGET_FRAME_TIME*1000;
    if(tdelta < target) {
      // TODO: using delayMicroseconds causes watchdog warnings
      // using vTaskDelay would solve this but causes inconsistent framerate
      delayMicroseconds(target - tdelta);
    }
    s.frame++;
    if(game_pause) score_screen();
  }
};

static void touch_poller(void *arg) {
  unsigned long q = 0;
  unsigned long p = 0;
  while(!game_quit) {
    touch_pad_read(TOUCH_PAD_NUM0, &game_tp[0]);
    touch_pad_read(TOUCH_PAD_NUM2, &game_tp[2]);
    touch_pad_read(TOUCH_PAD_NUM3, &game_tp[3]);
    touch_pad_read(TOUCH_PAD_NUM4, &game_tp[4]);
    touch_pad_read(TOUCH_PAD_NUM5, &game_tp[5]);
    touch_pad_read(TOUCH_PAD_NUM6, &game_tp[6]);
    touch_pad_read(TOUCH_PAD_NUM7, &game_tp[7]);
    touch_pad_read(TOUCH_PAD_NUM8, &game_tp[8]);
    vTaskDelay(10/portTICK_PERIOD_MS);
    if(game_tp[5] < 384) {
      q++;
    } else {
      q = 0;
    }
    if(q > 36) {
      game_quit = true;
      break;
    }
    if(game_tp[8] < 384) {
      p++;
    } else {
      p = 0;
    }
    if(p > 2) {
      game_pause = true;
    }
  }
}

static void intro_screen(void *arg) {
  tft->fillScreen(ST77XX_BLACK);
  tft->drawRect(84, 4, 32, 32, 0xfcc0);
  tft->fillRect(88, 8, 24, 24, 0xfcc0);
  tft->setTextSize(2);
  tft->setCursor(0, 4);
  tft->setTextColor(0xfcc0);
  tft->printf("Square\nDodge\n");
  tft->setTextSize(1);
  tft->setTextColor(0x738e);
  tft->printf("\nControls:\n");
  tft->setTextColor(0x029f);
  tft->printf("Press ");
  tft->setTextColor(0x1c00);
  tft->printf("S6");
  tft->setTextColor(0x09e0);
  tft->setTextColor(0x029f);
  tft->printf(" to start\nHold ");
  tft->setTextColor(0x1c00);
  tft->printf("S5");
  tft->setTextColor(0x09e0);
  tft->setTextColor(0x029f);
  tft->printf(" to quit\nPress ");
  tft->setTextColor(0x1c00);
  tft->printf("S8");
  tft->setTextColor(0x09e0);
  tft->setTextColor(0x029f);
  tft->printf(" to pause\n");
  tft->setTextColor(0x1c00);
  tft->printf("S0");
  tft->setTextColor(0x09e0);
  tft->printf("/");
  tft->setTextColor(0x1c00);
  tft->printf("S2");
  tft->setTextColor(0x09e0);
  tft->printf("/");
  tft->setTextColor(0x1c00);
  tft->printf("S3");
  tft->setTextColor(0x09e0);
  tft->printf("/");
  tft->setTextColor(0x1c00);
  tft->printf("S4");
  tft->setTextColor(0x09e0);
  tft->setTextColor(0x029f);
  tft->printf(" to move\n");
  tft->setTextColor(0x738e);
  tft->printf("\nObjective:\n");
  tft->setTextColor(0xf800);
  tft->printf("You are this:\n"); tft->fillRect(80, 99, 8, 8, ST77XX_WHITE); tft->fillRect(82, 101, 4, 4, ST77XX_RED);
  tft->setTextColor(0xfba0);
  tft->printf("Avoid getting hit by other squares.");

  unsigned long c = 0;
  while(1) {
    if(game_tp[6] < 384) {
      c++;
    } else {
      c = 0;
    }
    if(c > 4) break;
    vTaskDelay(10/portTICK_PERIOD_MS);
  }

  tft->fillScreen(ST77XX_BLACK);
}

static void game_task(void *arg) {
  SquareDodge *game = new SquareDodge(tft);
  intro_screen(NULL);
  while(!game_quit) {
    game->loop();
  }
  delete game;
  vTaskDelete(NULL);
}

void app_square_dodge_main(void) {
  vTaskDelete(badgeSleepTaskHandler);
  tft->setRotation(0);

  esp_err_t err = nvs_flash_init();
  if (err == ESP_ERR_NVS_NO_FREE_PAGES || err == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      err = nvs_flash_init();
  }
  ESP_ERROR_CHECK( err );

  game_quit = false;
  game_pause = false;
  for(int i = 0; i < TOUCH_PAD_MAX; i++) {
    game_tp[i] = 0;
  }
  tft->fillScreen(ST77XX_BLACK);
  TaskHandle_t Task1;
  xTaskCreatePinnedToCore(game_task, "game_task", 10000, NULL, 99, &Task1, 1);
  touch_poller(NULL);
  vTaskDelete(Task1);
  esp_restart();
}

/* 2019 Badge Example
 *    Unless required by applicable law or agreed to in writing, this
 *  software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *      CONDITIONS OF ANY KIND, either express or implied.
*/

#include <HTTPClient.h>
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_http_client.h"
#include "esp_https_ota.h"

#include "badgeWifi.h"
#include "badgeDisplay.h"
#include "badgePad.h"
#include "app_fox_submit.h"
#include "badgeStore.h"
#include "lwip/inet.h"
#include "lwip/ip4_addr.h"
#include "lwip/dns.h"


static const char *LOCALTAG = "badge19main_fox_submit";

enum BUTTON_NAMES {NONE=0,LEFT=0x01,MINUS=0x02,UP=0x04,DOWN=0x08,RIGHT=0x10,SELECT=0x20,START=0x40,A=0x80,B=0x100};

#define NAME_LENGTH_LIMIT 10

static char* editfirstname = NULL;
static char* editlastname = NULL;
static char* edithex = NULL;

static int cursor_x = 0;
static int cursor_y = 2;

static const unsigned int T0_MASK = 0x0001;
static const unsigned int T1_MASK = 0x0002;
static const unsigned int T2_MASK = 0x0004;
static const unsigned int T3_MASK = 0x0008;
static const unsigned int T4_MASK = 0x0010;
static const unsigned int T5_MASK = 0x0020;
static const unsigned int T6_MASK = 0x0040;
static const unsigned int T7_MASK = 0x0080;
static const unsigned int T8_MASK = 0x0100;
static const unsigned int T9_mask = 0x0200;

static bool displayChanged = false;

static void printstring(char* name, int cursor)
{
  int nameLength = strlen(name);
  for (int i = 0; i < nameLength && i < NAME_LENGTH_LIMIT; i++)
  {
    if (i == cursor) tft->setTextColor(ST77XX_BLACK, ST77XX_WHITE);
    tft->printf("%c",name[i]);
    if (i == cursor) tft->setTextColor(ST77XX_WHITE, ST77XX_BLACK);
  }
  tft->println("");
}

static void set_name_display()
{
  if (displayChanged)
  {
    tft->fillScreen(ST77XX_BLACK);
    tft->setTextSize(2);
    tft->setCursor(0,0);
    tft->setTextColor(ST77XX_BLUE, ST77XX_BLACK);
    if (edithex)
    {
      if (cursor_x == 0) printstring(edithex, cursor_y);
      else tft->printf("%s\n",edithex);
    }
    tft->println("");
    if (cursor_x == 1)
      tft->setTextColor(ST77XX_BLUE, ST77XX_WHITE);
    else
      tft->setTextColor(ST77XX_BLUE, ST77XX_BLACK);
    tft->println("Cancel");
    if (cursor_x == 2)
      tft->setTextColor(ST77XX_BLUE, ST77XX_WHITE);
    else
      tft->setTextColor(ST77XX_BLUE, ST77XX_BLACK); 
    tft->println("Submit");
    displayChanged = false;
  }
}

static char fn_default[NAME_LENGTH_LIMIT+1] = "My name is";
static char ln_default[NAME_LENGTH_LIMIT+1] = "Hacker\0\0\0\0";
static char hound_default[NAME_LENGTH_LIMIT+1] = "0x00000000";
bool bDNSFound = false;
static ip_addr_t ip_Addr; 
void dns_found_cb(const char *name, const ip_addr_t *ipaddr, void *callback_arg)
{
	ip_Addr = *ipaddr;
	bDNSFound = true;
}
static void load_hound_default()
{
  edithex = hound_default;
  if((editfirstname = retrieveString("name_storage","firstname",NAME_LENGTH_LIMIT,fn_default))
		  == NULL) {
	  editfirstname = fn_default;
  }
  if((editlastname = retrieveString("name_storage","lastname",NAME_LENGTH_LIMIT,ln_default))
		  == NULL) {
	  editlastname = ln_default;
  }

  for (int i = 0; i < NAME_LENGTH_LIMIT; i++)
  {
    if (editfirstname[i] == '\0') editfirstname[i] = ' ';
    if (editlastname[i] == '\0') editlastname[i] = ' ';
  }
}

// void initialise_wifi(void);
static void submit_code()
{
  initialise_wifi();
  ESP_LOGI(LOCALTAG, "Waiting for wifi to connect...");
  tft->println("ConnectN...");

  /* Wait for the callback to set the CONNECTED_BIT in the
     event group.
  */
  xEventGroupWaitBits(wifi_event_group, CONNECTED_BIT,
          false, true, portMAX_DELAY);
  // ESP_LOGI(LCOALTAG, "Connected to WiFi network! Attempting to connect to server...");
  char buffer[256] = {'\0'};
  /*for (int i = NAME_LENGTH_LIMIT-1; i; i--)
  {
    if (edithex[i] == ' ') edithex[i] = '\0';
    else break;
  }*/
  IP_ADDR4( &ip_Addr, 0,0,0,0 );
  bDNSFound = false;
  dns_gethostbyname("fox.zoo", &ip_Addr, dns_found_cb, NULL );
  uint64_t t = esp_timer_get_time();
  tft->println("DNSn...");
  while (!bDNSFound && esp_timer_get_time() < t + 10000000) // 10 seconds
  { vTaskDelay(100 / portTICK_PERIOD_MS); }
  if (bDNSFound) {
  	sprintf(buffer, "https://%i.%i.%i.%i:8080/%s|%s|%s",
		ip4_addr1(&ip_Addr.u_addr.ip4), 
		ip4_addr2(&ip_Addr.u_addr.ip4), 
		ip4_addr3(&ip_Addr.u_addr.ip4), 
		ip4_addr4(&ip_Addr.u_addr.ip4),
		edithex+2, editfirstname, editlastname);
  HTTPClient http;
  http.begin(buffer,(char *)ca_cert_pem);
  int httpCode = http.GET();
  if (httpCode > 0) { //Check for the returning code
   String payload = http.getString();
   // ESP_LOGI(LOCALTAG, httpCode);
   // ESP_LOGI(LCOALTAG, payload);
   http.end();
   tft->println("HTTP recv'd");
  }
 
  else {
     Serial.println("Error on HTTP request");
     tft->println("HTTP recv'd");
  }} else { Serial.println("Error on DNS request"); tft->println("DNS err"); }
}

static void set_name_task()
{
  bool quit=false;
  load_hound_default();
  unsigned int buttonState = 0;
  displayChanged = true;
  while(!quit)
  {    
    set_name_display();
    buttonState = readButtons();
    if (buttonState) {
      displayChanged = true;
    }
    
    if (buttonState == MINUS) {
        quit=true;
    } else if (buttonState & T0_MASK)
    {
      cursor_y -= 1;
      if (cursor_y < 2) {
        cursor_y = NAME_LENGTH_LIMIT-1;
      }
    } else if (buttonState & T2_MASK)
    {
      cursor_x -= 1;
      if (cursor_x < 0) {
        cursor_x = 2;
      }
    } else if (buttonState & T3_MASK)
    {
      cursor_x += 1;
      if (cursor_x >= 3) {
        cursor_x = 0;
      }
    } else if (buttonState & T4_MASK)
    {
      cursor_y += 1;
      if (cursor_y >= NAME_LENGTH_LIMIT) {
        cursor_y = 2;
      }
    } else if (buttonState & T7_MASK)
    {
      char tmp = 0;
      if (cursor_x == 0)
      {
        tmp = edithex[cursor_y];
        tmp += 1;
        if (tmp == ':') tmp = 'A';
        if (tmp == 'G') tmp = '0';
        edithex[cursor_y] = tmp;
      }
      if (cursor_x == 1)
      {
        quit = true;
      }
      else if (cursor_x == 2)
      {
        submit_code();
        vTaskDelay(5000 / portTICK_PERIOD_MS); //Don't check buttons too fast
        quit = true;
      }
    } else if (buttonState & T8_MASK)
    {
      char tmp = 0;
      if (cursor_x == 0)
      {
        tmp = edithex[cursor_y];
        tmp -= 1;
        if (tmp == '/') tmp = 'F';
        if (tmp == '@') tmp = '9';
        edithex[cursor_y] = tmp;
      }
    }
    vTaskDelay(100 / portTICK_PERIOD_MS); //Don't check buttons too fast
  }
}

void fox_submit_main()
{
  if (!Serial) { Serial.begin(115200); }
  while(!Serial) { vTaskDelay(100 / portTICK_PERIOD_MS); }
  Serial.println("Switching to set name");
  tft->fillScreen(ST77XX_BLACK);
  setStickyButtonsValue(false);
  setAppDelayReadValue(true);
  set_name_task();
  if(edithex != hound_default && edithex != NULL) free(edithex);
}

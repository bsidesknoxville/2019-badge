/* 2019 Badge Example
 *    Unless required by applicable law or agreed to in writing, this
 *    software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *    CONDITIONS OF ANY KIND, either express or implied.
 */

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "badgeWifi.h"

static const char *TAG = "badge19main_wifi";
const char * ca_cert_pem = "-----BEGIN CERTIFICATE-----\nMIIDojCCAoqgAwIBAgIJAIVwOs1fifhyMA0GCSqGSIb3DQEBCwUAMGYxCzAJBgNV\nBAYTAlVTMQswCQYDVQQIDAJUTjESMBAGA1UEBwwJS25veHZpbGxlMSEwHwYDVQQK\nDBhJbnRlcm5ldCBXaWRnaXRzIFB0eSBMdGQxEzARBgNVBAMMCnVwZGF0ZS56b28w\nHhcNMTkwNDI3MTcwMTA0WhcNMjAwNDI2MTcwMTA0WjBmMQswCQYDVQQGEwJVUzEL\nMAkGA1UECAwCVE4xEjAQBgNVBAcMCUtub3h2aWxsZTEhMB8GA1UECgwYSW50ZXJu\nZXQgV2lkZ2l0cyBQdHkgTHRkMRMwEQYDVQQDDAp1cGRhdGUuem9vMIIBIjANBgkq\nhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAt+YOf0UqQ3nuqeQem9s5mOnd4lf+gpmT\nwxHAuYYtFq2Uta3F9RXeXmTY/7mtIJel7Pdev35UJBzcNaBUR31FpqtCAS7KwgV+\nNEJMtSZdc06/uXbCnp8Rntw+ONAHrfhTMf4CL5clRgR/9rhjXItEksvXb/ZHnnBC\n5eahJ8SnJkMbhljePqSXC6x92kSLqWKemObaJvdVqngZms20/AsPlUCEdv+3wTvO\nj1rInYi/WBdAbzB5cgsPERPFzEzBibCSWw5e3MfwbV8rn4nn5vb/Y9m5wdnGfHgT\nAMZ1bbVTmQVA3BTTpWWh2RCoOelqjb2ZUjOKyjgfWTmG6TjHnz6ymQIDAQABo1Mw\nUTAdBgNVHQ4EFgQUbHykbziKsOfUuFYn5t+0Yj+9FOAwHwYDVR0jBBgwFoAUbHyk\nbziKsOfUuFYn5t+0Yj+9FOAwDwYDVR0TAQH/BAUwAwEB/zANBgkqhkiG9w0BAQsF\nAAOCAQEAWoTxu1JzFEsKty0roWo1zwMYRalZWx35vvINVHIzCM//sMBJFHF+hzi0\n34oHcW7SbsB0GbOt/cCynfOfVgQgHdSC/sontGjh/cwLXJIuNxdbHRABeFg136BR\nPrmR9isCPugqG11CALpOw9hHIQ5Pbt+2AYvlh2M2vU/RHJl1naP0QAcrH/qo+Zd9\nv3n+jX85jy5HTBMf8+yd0ySprPVFNTapnsj+NRmLoQTNBs3ceKFljX0iT+ArzOTA\n/WSLq8WKnv4ylYT+eD5IwuoLIDHGgnqZxanW4lJqEqJvOcRd3iqanEMGIf9YF9uJ\nDN+xwHd2L+yazlcKJrzF+2pVT758iA==\n-----END CERTIFICATE-----";

/* FreeRTOS event group to signal when we are connected & ready to make a request */
EventGroupHandle_t wifi_event_group;

/* The event group allows multiple bits for each event,
   but we only care about one event - are we connected
   to the AP with an IP? */
const int CONNECTED_BIT = BIT0;

static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch (event->event_id) {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        xEventGroupSetBits(wifi_event_group, CONNECTED_BIT);
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        /* This is a workaround as ESP32 WiFi libs don't currently
           auto-reassociate. */
        esp_wifi_connect();
        xEventGroupClearBits(wifi_event_group, CONNECTED_BIT);
        break;
    default:
        break;
    }
    return ESP_OK;
}

void initialise_wifi(void)
{
    tcpip_adapter_init();
    if (!wifi_event_group) { wifi_event_group = xEventGroupCreate();
    ESP_ERROR_CHECK( esp_event_loop_init(event_handler, NULL) ); }
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK( esp_wifi_init(&cfg) );
    ESP_ERROR_CHECK( esp_wifi_set_storage(WIFI_STORAGE_RAM) );
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = CONFIG_WIFI_SSID,
            .password = CONFIG_WIFI_PASSWORD,
        },
    };
    ESP_LOGI(TAG, "Setting WiFi configuration SSID %s", wifi_config.sta.ssid);
    ESP_ERROR_CHECK( esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK( esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK( esp_wifi_start() );
}

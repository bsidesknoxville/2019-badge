/* 2019 Badge Example
 *    Unless required by applicable law or agreed to in writing, this
 *    software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *    CONDITIONS OF ANY KIND, either express or implied.
*/
#ifndef BADGESTORE
#define BADGESTORE
#if defined (__cplusplus)
extern "C" {
#endif
void storeString(const char *store,const char *str,char *val);
char *retrieveString(const char *store,const char *str,
		const int max_size,const char *default_str);
void badgeStoreInit();
#if defined (__cplusplus)
}
#endif
#endif

/* 2019 Badge Example
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"


#include "badgePad.h"
#include "badgeDisplay.h"

#include "app_sched.h"
#include "cJSON.h"

enum BUTTON_NAMES {NONE=0,LEFT=0x01,MINUS=0x02,UP=0x04,DOWN=0x08,RIGHT=0x10,SELECT=0x20,START=0x40,A=0x80,B=0x100};

#include "app_sched_default.h"

static const char *TAG = "sched";

/*       | Month   | Day     | Offset | Duration
 * Size  | 4 bits  | 5 bits  | 6 bits | 4 bits
 * Mask  | 0x7ffff | 0x7fff  | 0x3ff  | 0xf
 * Shift | 15 bits | 10 bits | 4 bits | 0 bits
 * 
 * Offset specifies how many 15 minute blocks till talk
 * start time from 08:00. Duration is also specifies
 * how many 15 minute blocks are in each talk.
 */
static int metaMonth(int meta) {
	return meta>>15;
}
static int metaDay(int meta) {
	return (meta&0x7fff)>>10;
}
static int metaHour(int meta) {
	return 8+(((meta&0x3ff)>>4)/4);
}
static int metaMin(int meta) {
	return (((meta&0x3ff)>>4)%4)*15;
}
static int metaDur(int meta) {
	return (meta&0xf)*15;
}

static cJSON *root,*tracks,*track,*trackName,*talks,*talk,*talkMeta,*talkTitle,*talkDesc;
static int font_size = 2;

static const int max_track_length = 8;
static char track_buffer[max_track_length+1];

static bool selected;

static const int max_desc_length = 1024;
static const int max_title_length = 128;
static char title_buffer[max_title_length+1];
static char desc_buffer[max_desc_length+1];

static char * title_pointer = title_buffer;
static char * desc_pointer = desc_buffer;

static const int line_length[] = {21,10,7,5,4};
static const int num_lines[] = {10,5,3,2,2};

static void updateSched() {
	tft->fillScreen(ST77XX_BLACK);
	tft->setCursor(4,0);
	tft->setTextColor(ST77XX_WHITE, ST77XX_BLUE);
	tft->setTextSize(2);
	tft->println("2019 SCHED");
	tft->setCursor(tft->getCursorX()+4,tft->getCursorY());
	if(selected) {
		tft->setTextColor(ST77XX_RED,ST77XX_BLACK);
	} else {
		tft->setTextColor(ST77XX_GREEN,ST77XX_RED);
	}
	tft->printf("%.4s %02i/%02i\n",track_buffer,metaMonth(talkMeta->valueint),metaDay(talkMeta->valueint));
	tft->setCursor(tft->getCursorX()+4,tft->getCursorY());
	tft->printf("%.4s %02i:%02i\n",track_buffer+4,metaHour(talkMeta->valueint),metaMin(talkMeta->valueint));
	tft->setTextSize(font_size);
	tft->setTextColor(ST77XX_GREEN,ST77XX_BLACK);
	if(selected) {
		switch (font_size) {
			case 1: tft->printf("%-210s..",desc_pointer); break;
			case 2: tft->printf("%-50s..",desc_pointer); break;
			case 3: tft->printf("%-21s..",desc_pointer); break;
			case 4: tft->printf("%-10s..",desc_pointer); break;
			case 5: tft->printf("%-8s..",desc_pointer);
		}
	} else {
		switch (font_size) {
			case 1: tft->printf("%-210s..",title_pointer); break;
			case 2: tft->printf("%-50s..",title_pointer); break;
			case 3: tft->printf("%-21s..",title_pointer); break;
			case 4: tft->printf("%-10s..",title_pointer); break;
			case 5: tft->printf("%-8s..",title_pointer);
		}
	}
	ESP_LOGI(TAG,"Track: %s Date: %02i/%02i\nTime: %02i:%02i Length: %i min\n\t%s\n\n\t%s",
		trackName->valuestring,
		metaMonth(talkMeta->valueint),
		metaDay(talkMeta->valueint),
		metaHour(talkMeta->valueint),
		metaMin(talkMeta->valueint),
		metaDur(talkMeta->valueint),
		talkTitle->valuestring,
		talkDesc->valuestring);
}

static bool quit;

static void newTalk() {
	int z;
	talkMeta = cJSON_GetObjectItem(talk,"meta");
	talkTitle = cJSON_GetObjectItem(talk,"title");
	talkDesc = cJSON_GetObjectItem(talk,"desc");
	selected = false;
	for(z=0;(z<(max_title_length+1)) && ((talkTitle->valuestring)[z] != '\0');z++) {
		title_buffer[z] = (talkTitle->valuestring)[z];
	} for (;z<max_title_length+1;z++) { title_buffer[z] = '\0'; }
	for(z=0;(z<(max_desc_length+1)) && ((talkDesc->valuestring)[z] != '\0');z++) {
		desc_buffer[z] = (talkDesc->valuestring)[z];
	} for (;z<max_desc_length+1;z++) { desc_buffer[z] = '\0'; }
	title_pointer = title_buffer;
	desc_pointer = desc_buffer;
}

static void newTrack() {
	int z;
	trackName = cJSON_GetObjectItem(track,"name");
	talks = cJSON_GetObjectItem(track,"talks");
	talk = talks->child;
	for(z=0;(z<(max_track_length+1)) && ((trackName->valuestring)[z] != '\0');z++) {
		track_buffer[z] = (trackName->valuestring)[z];
	} for (;z<max_track_length+1;z++) { track_buffer[z] = '\0'; }
	newTalk();
}

static void onLeft() { if(talk->prev) { talk = talk->prev; newTalk(); updateSched(); } } // Previous Talk 
static void onMinus() { quit = true; } // Exit
static void onDown() { if(selected) { 
		if(desc_pointer < desc_buffer + strlen(desc_buffer) - line_length[font_size-1]*(num_lines[font_size-1]-1)) {
			desc_pointer+=line_length[font_size-1]; updateSched(); } // Navigate Talk Desc Down
	} else { 
		if(title_pointer < title_buffer + strlen(title_buffer) - line_length[font_size-1]*(num_lines[font_size-1]-1)) {
			title_pointer+=line_length[font_size-1]; updateSched(); }
	}
} // Navigate Talk Desc Down
static void onUp() { if(selected) {
	if (desc_pointer != desc_buffer) {
		desc_pointer = desc_buffer; updateSched(); }
	} else { if (title_pointer != title_buffer) {
		title_pointer = title_buffer; updateSched(); }
	}
} // Navigate Talk Desc Up
static void onRight() { if(talk->next) { talk = talk->next; newTalk(); updateSched(); } } // Next Talk
static void onSelect() { if(font_size > 1) { font_size--; updateSched(); } } // Font Size Down
static void onStart() { if(font_size < 5) { font_size++; updateSched(); } } // Font Size Up
static void onB() { if(track->next) { track = track->next; }
	else { while(track->prev) { track = track->prev; } } newTrack(); updateSched(); } // Next Track
static void onA() { selected = !selected; updateSched(); } // Un/Select Trackk


static void controller() {
	uint32_t buttons;
	if ((buttons = readButtons())) {
		if (buttons == 0xffff) { updateSched(); } // Back from light sleep mode
		else if (buttons & LEFT) { onLeft(); }
		else if (buttons & MINUS) { onMinus(); }
		else if (buttons & UP) { onUp(); }
		else if (buttons & DOWN) { onDown(); }
		else if (buttons & RIGHT) { onRight(); }
		else if (buttons & SELECT) { onSelect(); }
		else if (buttons & START) { onStart(); }
		else if (buttons & A) { onA(); }
		else if (buttons & B) { onB(); }
		if ((buttons & (SELECT | START)) == (SELECT | START)) { 
		}
	}
}

static void initSchedSettings() {
	track = tracks->child;
	newTrack();
}

static bool testJSON(void) {
	char *printed;
	int meta;
	if (!cJSON_IsObject(root)) {
		ESP_LOGI(TAG,"JSON read error on '%s'", sched_default);
		return false;
	}
	printed = cJSON_Print(root);
	ESP_LOGI(TAG,"JSON read successfully:\n%s", printed);
	free(printed);
	tracks = cJSON_GetObjectItem(root,"tracks");
	if (!cJSON_IsArray(tracks)) {
		ESP_LOGI(TAG,"Error extracting tracks");
		return false;
	}
	cJSON_ArrayForEach(track, tracks) {
		if(!cJSON_IsObject(track)) {
			ESP_LOGI(TAG,"Error extracting track");
			return false;
		}
		trackName = cJSON_GetObjectItem(track,"name");
		talks = cJSON_GetObjectItem(track,"talks");
		if(!cJSON_IsString(trackName) || !cJSON_IsArray(talks)) {
			ESP_LOGI(TAG,"Error extracting track data");
			return false;
		}
		cJSON_ArrayForEach(talk, talks) {
			talkMeta = cJSON_GetObjectItem(talk,"meta");
			talkTitle = cJSON_GetObjectItem(talk,"title");
			talkDesc = cJSON_GetObjectItem(talk,"desc");
			if(!cJSON_IsNumber(talkMeta) || !cJSON_IsString(talkTitle) || !cJSON_IsString(talkDesc)) {
				ESP_LOGI(TAG,"Error extracting talk");
				return false;
			}
			meta = talkMeta->valueint;
			ESP_LOGI(TAG,"Track: %s, Date: %02i/%02i, Time: %02i:%02i, Length: %i min\n\t%s\n\t\t%s",
				trackName->valuestring,
				metaMonth(meta),
				metaDay(meta),
				metaHour(meta),
				metaMin(meta),
				metaDur(meta),
				talkTitle->valuestring,
				talkDesc->valuestring);
		}
	}
	return true;
}

void app_sched_main(void) {
	ESP_LOGI(TAG,"Initiating sched");
	setStickyButtonsValue(false);
	setAppDelayReadValue(true);
	tft->fillScreen(ST77XX_BLACK);
	tft->setCursor(4,0);
	tft->setTextColor(ST77XX_WHITE, ST77XX_BLUE);
	tft->setTextSize(2);
	tft->println("2019 SCHED");
	tft->setCursor(tft->getCursorX()+4,tft->getCursorY());
	tft->setTextColor(ST77XX_WHITE, ST77XX_RED);
	tft->setTextSize(3);
	tft->println("LoadN..");

	root = cJSON_Parse(sched_default);
	if (testJSON()) {
		initSchedSettings();
		updateSched();
		quit = false;
		vTaskDelay(1000 / portTICK_PERIOD_MS); // Wait for input from menu selection to end
		while(!quit) { controller(); vTaskDelay(100 / portTICK_PERIOD_MS); }
	}
	ESP_LOGI(TAG,"Quitting sched");
	cJSON_Delete(root);
}

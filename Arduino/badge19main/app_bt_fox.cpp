/* 
Based on ESP32>BLE_server_multiconnect
    Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleNotify.cpp
    Ported to Arduino ESP32 by Evandro Copercini
    updated by chegewara
converted to badge app by padewitt
*/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "badgePad.h"
#include "badgeDisplay.h"
#include "app_bt_fox.h"
//TaskHandle_t badgeFoxTaskHandler=NULL;
static bool isFoxRunning=false;

enum BUTTON_NAMES {NONE=0,LEFT=0x01,MINUS=0x02,UP=0x04,DOWN=0x08,RIGHT=0x10,SELECT=0x20,START=0x40,A=0x80,B=0x100};
static const char *TAG = "bt_fox";

void start_fox();
void stop_fox();

static void updateBTFoxScreen() {
    tft->fillScreen(ST77XX_BLACK);
    tft->setCursor(0,0);
    tft->setTextColor(ST77XX_BLUE, ST77XX_BLACK);
    tft->println("Fox is ");
    if(isFoxRunning) {
        tft->println("Active");
        tft->println("Press S8");
        tft->println("to stop");
    } else {
        tft->println("Inactive");
        tft->println("Press S7");
        tft->println("to start");
    }
}
static bool quit;
static void controller() {
    uint32_t buttons;
    if ((buttons = readButtons())) {
        ESP_LOGI(TAG,"Button pressed: %i",buttons);
        if (buttons == 0xffff) { updateBTFoxScreen(); } // Back from light sleep mode
        else {
            if (buttons & MINUS) { quit = true; }
            if (buttons & A) { start_fox(); updateBTFoxScreen(); }
            if (buttons & B) { stop_fox(); updateBTFoxScreen(); }
            if ((buttons & (SELECT | START)) == (SELECT | START)) { quit = true; }
        }
    }
}

#include <BLEDevice.h>
#include <BLEServer.h>
#include <BLEUtils.h>
#include <BLE2902.h>

BLEServer* pServer = NULL;
BLECharacteristic* pCharacteristic = NULL;
bool deviceConnected = false;
bool oldDeviceConnected = false;
uint32_t value = 0;

// See the following for generating UUIDs:
// https://www.uuidgenerator.net/

#define SERVICE_UUID        "00b51de0-7da2-443c-a932-3d40227afabb"
#define CHARACTERISTIC_UUID "00b51de1-7da2-443c-a932-3d40227afabb"


class MyServerCallbacks: public BLEServerCallbacks {
    void onConnect(BLEServer* pServer) {
      deviceConnected = true;
      BLEDevice::startAdvertising();
    };

    void onDisconnect(BLEServer* pServer) {
      deviceConnected = false;
    }
};



void foxSetup() {
  Serial.begin(115200);

  // Create the BLE Device
  BLEDevice::init("Bluetooth Fox");

  // Create the BLE Server
  pServer = BLEDevice::createServer();
  pServer->setCallbacks(new MyServerCallbacks());

  // Create the BLE Service
  BLEService *pService = pServer->createService(SERVICE_UUID);

  // Create a BLE Characteristic
  pCharacteristic = pService->createCharacteristic(
                      CHARACTERISTIC_UUID,
                      BLECharacteristic::PROPERTY_READ   |
                      BLECharacteristic::PROPERTY_WRITE  |
                      BLECharacteristic::PROPERTY_NOTIFY |
                      BLECharacteristic::PROPERTY_INDICATE
                    );

  // https://www.bluetooth.com/specifications/gatt/viewer?attributeXmlFile=org.bluetooth.descriptor.gatt.client_characteristic_configuration.xml
  // Create a BLE Descriptor
  pCharacteristic->addDescriptor(new BLE2902());

  // Start the service
  pService->start();

  // Start advertising
  BLEAdvertising *pAdvertising = BLEDevice::getAdvertising();
  pAdvertising->addServiceUUID(SERVICE_UUID);
  pAdvertising->setScanResponse(false);
  pAdvertising->setMinPreferred(0x0);  // set value to 0x00 to not advertise this parameter
  BLEDevice::startAdvertising();
  Serial.println("Waiting a client connection to notify...");
}

void app_bt_fox_main(void) {
    ESP_LOGI(TAG,"Initiating");
    updateBTFoxScreen();
    quit = false;
    while(!quit) {
        controller();
        delay(100);
    }
    ESP_LOGI(TAG,"Quitting");  
}

void start_fox() {
    if(!isFoxRunning) {
        foxSetup();
        isFoxRunning = true;
    }
}

void stop_fox() {
    if (isFoxRunning) {
        BLEDevice::deinit(true);
    }
    isFoxRunning = false;
}

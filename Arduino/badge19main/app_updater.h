/* 2019 Badge Example
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#ifndef APP_UPDATER
#define APP_UPDATER
#if defined (__cplusplus)
extern "C" {
#endif
void app_updater_main(void);
void update_badge19(void);
#if defined (__cplusplus)
}
#endif
#endif



/* 2019 Badge Bluetooth fox
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#if defined (__cplusplus)
extern "C" {
#endif
void app_bt_fox_main(void);
#if defined (__cplusplus)
}
#endif


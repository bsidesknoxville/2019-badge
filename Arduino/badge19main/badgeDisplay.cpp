/* 2019 Badge Example
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "badgeDisplay.h"
#include "badgeStore.h"

SPIClass * vspi = NULL;
Adafruit_ST7735 * tft = NULL;

void badgeDisplayInit(void) {
	vspi = new SPIClass(VSPI);
	vspi->begin(18, 19, 23, -1); //Remap the MISO pin to be an unused pin instead of the default of pin 5
	tft = new Adafruit_ST7735(vspi, CS_D, DC_D, RESET_D);
	tft->initR(INITR_144GREENTAB); // Init ST7735R chip, green tab
}

static uint8_t prevRotation;

uint16_t convertRGB( uint8_t R, uint8_t G, uint8_t B)
{
	  return ( ((R & 0xF8) << 8) | ((G & 0xFC) << 3) | (B >> 3) );
}

void drawSleepDisplay(void) {
	char *firstname = retrieveString("name_storage","firstname",10,"My name is");
	char *lastname = retrieveString("name_storage","lastname",10,"Hacker\0\0\0\0");
	prevRotation = tft->getRotation();
	tft->setRotation(3);
	tft->fillScreen(0x0000);
	tft->setCursor(6,40);
	tft->setTextColor(convertRGB(0,196,0), 0x0000);
	tft->setTextSize(2);
	if (firstname) {
		tft->printf("%.10s",firstname);
		free(firstname);
	} else {
		tft->printf("%.10s","My name is");	
	}
	tft->setCursor(6,70);
	if (firstname) {
		tft->printf("%.10s",lastname);
		free(lastname);
	} else {
		tft->printf("%.10s","Hacker");	
	}
}

void cleanUpSleepDisplay(void) {
	tft->setRotation(prevRotation);
}

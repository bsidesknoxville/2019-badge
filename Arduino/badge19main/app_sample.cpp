/* 2019 Badge Example
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "badgePad.h"
#include "badgeDisplay.h"

#include "app_sample.h"
enum BUTTON_NAMES {NONE=0,LEFT=0x01,MINUS=0x02,UP=0x04,DOWN=0x08,RIGHT=0x10,SELECT=0x20,START=0x40,A=0x80,B=0x100};
static const char *TAG = "sample";

static int x = 0, y = 0, z = 2;
static uint16_t fg = 0xffff, bg = 0x0000;
static void updateSampleScreen() {
	tft->fillScreen(ST77XX_BLACK);
	tft->setCursor(x,y);
	tft->setTextColor(fg, bg);
	tft->setTextSize(z);
	tft->println("==SAMPLE==");
}

static bool quit;

static void onLeft() { if(x) { x--; } updateSampleScreen(); }
static void onMinus() { fg = ~fg; bg = ~bg; updateSampleScreen(); }
static void onUp() { if(y) { y--; } updateSampleScreen(); }
static void onDown() { if(y<127) { y++; } updateSampleScreen(); }
static void onRight() { if(x<127) { x++; } updateSampleScreen(); }
static void onSelect() { fg <<= 1; bg >>= 1; updateSampleScreen(); }
static void onStart() { fg >>= 1; bg <<= 1; updateSampleScreen(); }
static void onA() { fg++; bg--; updateSampleScreen(); }
static void onB() {fg--; bg++; updateSampleScreen(); }


static void controller() {
	uint32_t buttons;
	if ((buttons = readButtons())) {
		ESP_LOGI(TAG,"Button pressed: %i",buttons);
		if (buttons == 0xffff) { updateSampleScreen(); } // Back from light sleep mode
		else {
			if (buttons & LEFT) { onLeft(); }
			if (buttons & MINUS) { onMinus(); }
			if (buttons & UP) { onUp(); }
			if (buttons & DOWN) { onDown(); }
			if (buttons & RIGHT) { onRight(); }
			if (buttons & SELECT) { onSelect(); }
			if (buttons & START) { onStart(); }
			if (buttons & A) { onA(); }
			if (buttons & B) { onB(); }
			if ((buttons & (SELECT | START)) == (SELECT | START)) { quit = true; }
		}
	}
}

void app_sample_main(void) {
	ESP_LOGI(TAG,"Initiating");
	updateSampleScreen();
	setStickyButtonsValue(true);
	setAppDelayReadValue(true);
	quit = false;
	while(!quit) { controller(); vTaskDelay(100 / portTICK_PERIOD_MS); }
	ESP_LOGI(TAG,"Quitting");  
}

/*

Make a YourApp.cpp file
This is where your app will live.

Include these headers:

#include "badgeDisplay.h"
#include "badgePad.h"
#include "YourApp.h" // Really, your .h file

Create 3 functions

Display, where you add any drawing commands.
static void hello_display()
{
  tft->fillScreen(ST77XX_BLACK);
  tft->setCursor(0,0);
  tft->setTextColor(ST77XX_GREEN, ST77XX_BLACK);
  tft->setTextSize(1);
  tft->println("Hello World");
}


Task.  This is the main loop.  When it returns, the screen will return to the menu
static void hello_task()
{
    int i;
    for(i=0; i<10; i++) {
        delay(1000);
    }
}

Init, this is what's called from the main menu to initialize the task.  Do any
initilizing here.

void hello_init()
{
  Serial.println("Switching to hello");
  tft->fillScreen(ST77XX_BLACK);
  hello_task(); // start your main loop
}


### Make .h file

Put prototypes of your functions here
void hello_init();

### main_menu changes

Add your .h file
#include "hello.h"

Increase the menu count (it was 2 before):
int8_t menu_count = 3;

Add your entry to the main_menu list.  The string is the name that shows in the
menu, then a color to display the text, then the name of your menu function
from before.

menu_entry main_menu[] = 
...
, {"Hello", ST77XX_GREEN, hello_menu}
...

*/

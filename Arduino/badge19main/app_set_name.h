/* 2019 Badge Example
 *    Unless required by applicable law or agreed to in writing, this
 *    software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
 *    CONDITIONS OF ANY KIND, either express or implied.
*/
#ifndef APP_SET_NAME
#define APP_SET_NAME
#if defined (__cplusplus)
extern "C" {
#endif
void set_name_main();
void set_name_init();
#if defined (__cplusplus)
}
#endif
#endif

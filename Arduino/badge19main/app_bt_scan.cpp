/**
  Based on example in ESP32>BLE_Scan:
  Based on Neil Kolban example for IDF: https://github.com/nkolban/esp32-snippets/blob/master/cpp_utils/tests/BLE%20Tests/SampleScan.cpp
  Ported to Arduino ESP32 by Evandro Copercini

  Converted to badge app by padewitt
 **/
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "badgePad.h"
#include "badgeDisplay.h"
#include "app_bt_scan.h"

#include <BLEDevice.h>
//#include <BLEUtils.h>
//#include <BLEScan.h>
//#include <BLEAdvertisedDevice.h>
enum BUTTON_NAMES {NONE=0,LEFT=0x01,MINUS=0x02,UP=0x04,DOWN=0x08,RIGHT=0x10,SELECT=0x20,START=0x40,A=0x80,B=0x100};
static const char *TAG = "bt_scan";
static uint32_t Scan_device_count = 0;
static uint32_t Scan_fox_count = 0;

static uint16_t fg = 0xffff, bg = 0x0000;
static void updateBTScanScreen() {
    tft->fillScreen(ST77XX_BLACK);
    tft->setCursor(0,0);
    tft->setTextColor(ST77XX_BLUE, ST77XX_BLACK);
    tft->println("Devices: ");
    tft->println(Scan_device_count);
    tft->println("Foxes: ");
    tft->println(Scan_fox_count);
}
static bool quit;
static void controller() {
    uint32_t buttons;
    if ((buttons = readButtons())) {
        ESP_LOGI(TAG,"Button pressed: %i",buttons);
        if (buttons == 0xffff) { updateBTScanScreen(); } // Back from light sleep mode
        else {
            if (buttons & MINUS) { quit = true; }
            if ((buttons & (SELECT | START)) == (SELECT | START)) { quit = true; }
        }
    }
}

int scanTime = 3; //In seconds
BLEScan* pBLEScan;

class MyAdvertisedDeviceCallbacks: public BLEAdvertisedDeviceCallbacks {
    void onResult(BLEAdvertisedDevice advertisedDevice) {
        Serial.printf("Advertised Device: %s \n", advertisedDevice.toString().c_str());
        if (advertisedDevice.getName() == "Bluetooth Fox") {
            Scan_fox_count++;
        }
    }
};

void bt_setup() {
    Serial.begin(115200);
    Serial.println("Scanning...");

    BLEDevice::init("");
    pBLEScan = BLEDevice::getScan(); //create new scan
    pBLEScan->setAdvertisedDeviceCallbacks(new MyAdvertisedDeviceCallbacks());
    pBLEScan->setActiveScan(true); //active scan uses more power, but get results faster
    pBLEScan->setInterval(100);
    pBLEScan->setWindow(99);  // less or equal setInterval value
    updateBTScanScreen();
}

void bt_loop() {
    // put your main code here, to run repeatedly:
    Scan_fox_count = 0;
    BLEScanResults foundDevices = pBLEScan->start(scanTime, false);
    Scan_device_count = foundDevices.getCount();
    updateBTScanScreen();
    Serial.print("Devices found: ");
    Serial.println(foundDevices.getCount());
    Serial.println("Scan done!");
    pBLEScan->clearResults();   // delete results fromBLEScan buffer to release memory
    delay(200);
}

void app_bt_scan_main(void) {
    ESP_LOGI(TAG,"Initiating");
    bt_setup();
    quit = false;
    while(!quit) {
        controller();
        bt_loop();
    }
    ESP_LOGI(TAG,"Quitting");  
}

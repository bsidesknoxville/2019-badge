/* 2019 Badge Example
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "badgePad.h"
#include "badgeMenu.h"
#include "badgeDisplay.h"

enum BUTTON_NAMES {NONE=0,LEFT=0x01,MINUS=0x02,UP=0x04,DOWN=0x08,RIGHT=0x10,SELECT=0x20,START=0x40,A=0x80,B=0x100};

int16_t default_fg = ST77XX_WHITE;
int16_t default_bg = ST77XX_BLACK;

struct menu_entry
{
	  char entry[11];
	  int fg_color;
	  void (*callback)(void);
};

static int8_t menu_count = 11;
static int8_t menu_index = 0;

#include "app_smile.h"
#include "app_name_and_time.h"
#include "app_set_name.h"
#include "app_sched.h"
#include "app_sample.h"
#include "app_updater.h"
#include "app_bt_scan.h"
#include "app_bt_fox.h"
#include "app_square_dodge.h"
#include "app_fox_submit.h"
#include "app_flappy.h"
#include "app_gif.h"

static menu_entry main_menu[] =
	{ {"=Schedule=", ST77XX_YELLOW, app_sched_main}
	, {"Fox Hunter", ST77XX_BLUE, app_bt_scan_main}
	, {"=Set Name=", ST77XX_RED, set_name_main}
	, {"FoundA Fox", ST77XX_BLUE, fox_submit_main}
	, {"==SqDodge=", 0xfcc0, app_square_dodge_main}
	, {"=GIF Play=", ST77XX_YELLOW, gif_main}
	, {"==Smile===", ST77XX_CYAN, smile_main}
	, {"=Name Time", ST77XX_GREEN, name_time_main}
	, {"==Sample==", ST77XX_MAGENTA, app_sample_main}
	, {"==Flappy==", ST77XX_BLUE, flappy_main}
	, {"==Updater=", ST77XX_WHITE, app_updater_main}};

static void updateMenu() {
	tft->fillScreen(ST77XX_BLACK);
	tft->setCursor(3,0);
	tft->setTextColor(ST77XX_WHITE, ST77XX_BLUE);
	tft->setTextSize(2);
	tft->println("+2019 MENU");
	tft->setTextColor(ST77XX_WHITE);
	for (int i, j = i = max(0,menu_index - 6); i < j+7; i++)
	{
		int fg = default_fg;
		if (main_menu[i].fg_color) { fg = main_menu[i].fg_color; }
		if (i == menu_index)
		{
			tft->setTextColor(~fg, ~default_bg);
		}
		else
		{
			tft->setTextColor(fg, default_bg);
		}
		tft->println(main_menu[i].entry);
	}
}

static void initMenu() {
	tft->setRotation(0);
	setStickyButtonsValue(true);
	setAppDelayReadValue(true);
	updateMenu();
}

static void onUp() {
	if(menu_index) { menu_index -= 1; updateMenu(); } 
}
static void onDown() { 
	if(menu_index < (menu_count - 1)) { menu_index += 1; updateMenu(); }
}

static void onA() {
	if (main_menu[menu_index].callback) {
		main_menu[menu_index].callback();
	}
	initMenu();
}

static void controller() {
	uint32_t buttons;
	if ((buttons = readButtons())) {
		if (buttons == 0xffff) { updateMenu(); }
		else if (buttons & UP) { onUp(); }
		else if (buttons & DOWN) { onDown(); }
		else if (buttons & A) { onA(); }
	}
}

void badgeMainTask(void *pvParamter) {
	initMenu();
	while(1) { controller(); vTaskDelay(100 / portTICK_PERIOD_MS); }
	vTaskDelete(NULL);
}

/* 2019 Badge Example
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#ifndef BADGEPAD
#define BADGEPAD
#if defined (__cplusplus)
extern "C" {
#endif
void setInterruptForBadge(uint8_t pin, void (*userFunc)(void));

void setStickyButtonsValue(bool val);
void setAppDelayReadValue(bool val); 
void setStickyButtonsTime(uint64_t t);
void setAppDelayReadTime(uint64_t t);

uint32_t readButtons();
void badgePadInit();
void badgeSleepTask(void *);
#if defined (__cplusplus)
}
#endif
#endif

/* 2019 Badge Example
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

/* Button Handler (Use this for your App Controller)
 *
 * Interrupts can be set per button with the 
 * setInterruptForBadge function or the state can be
 * read via the readButtons function. Two boolean
 * values exist to adjust how readButtons functions:
 * stickyButtons and appDelayRead. stickyButtons
 * when set to true (it is false by default) will
 * make sure successive reads will result in no new
 * button presses if a user sustains a push.
 * appDelayRead when set to true (it is true by
 * default) will cause a readButtons call to ignore
 * button pushes if the call occurs too late after
 * the push occurred. Set these values via the
 * following functions: setAppDelayReadValue and
 * setStickyButtonsValue. You can also set the times
 * related with the features with the setAppDelayReadTime
 * and setStickyButtonsTime functions.
 */

#include "driver/touch_pad.h"
#include "Arduino.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <driver/rtc_io.h>
#include "badgeDisplay.h"
#include "badgePad.h"

static void (*buttonInterruptUserFuncs[TOUCH_PAD_MAX - 1])(void) = {};

void setInterruptForBadge(uint8_t pin, void (*userFunc)(void)) {
	uint16_t thresh;
	buttonInterruptUserFuncs[pin] = userFunc;
	touch_pad_get_thresh(pin,&thresh);
	touchAttachInterrupt(pin, userFunc, thresh);
}

static bool stickyButtons = false;
static bool appDelayRead = true;
static uint64_t stickyButtonsTime = 75000;
static uint64_t appDelayReadTime = 200000;
void setStickyButtonsValue(bool val) { stickyButtons = val; }
void setAppDelayReadValue(bool val) { appDelayRead = val; }
void setStickyButtonsTime(uint64_t t) { stickyButtonsTime = t; }
void setAppDelayReadTime(uint64_t t) { appDelayReadTime = t; }

static uint32_t buttonState;
static uint64_t buttonTriggerTime[TOUCH_PAD_MAX - 1];

uint32_t readButtons() {
    uint32_t val = 0;
    uint64_t t;
    if (appDelayRead && buttonState != 0xffff) {
    	t = esp_timer_get_time();
    	for (int i = 0; i < TOUCH_PAD_MAX - 1; i++) {
		if ((buttonState & (0x01 << i))) {
			if (t < (buttonTriggerTime[i] + appDelayReadTime)) {
				val |= 0x01 << i;
			}
			buttonState ^= 0x01 << i;
         	}
    	}
    	return val;
    }
    val = buttonState;
    buttonState = 0;
    return val;
}

static void badgePadInterrupt(void * arg)
{
    bool cleared = true;
    if (buttonState) cleared = false;
    uint32_t currentButtonState = (buttonState = touch_pad_get_status());
    uint64_t t;
    touch_pad_clear_status();
    t = esp_timer_get_time();
    for (int i = 0; i < TOUCH_PAD_MAX - 1; i++) {
    	if ((currentButtonState >> i) & 0x01) {
     		if(!stickyButtons && cleared && (buttonTriggerTime[i] + stickyButtonsTime) > t) { 
    			currentButtonState ^= (0x01 << i);
    		}
        	buttonTriggerTime[i] = t;
    	}
    }
    if(!stickyButtons) {
	    buttonState = currentButtonState;
    }
}


static uint64_t sleepDelayTime = 90000000;
static bool deepSleepMode = true; // false is broken

void sleepWakeUp(void) {
}

void badgeSleepTask(void *pvParamter) {	
	int i;
    	uint64_t t;
	while (1) {
		vTaskDelay((sleepDelayTime / 1000) / portTICK_PERIOD_MS);
    		t = esp_timer_get_time();	
		for(i=0;i<TOUCH_PAD_MAX-1;i++) {
			if((buttonTriggerTime[i]+sleepDelayTime)>t) {
				break;
			}
		}
		if(i == (TOUCH_PAD_MAX - 1)) {
			touch_pad_intr_disable();
			drawSleepDisplay();
			touchAttachInterrupt(T1, sleepWakeUp,20);
			rtc_gpio_hold_en((gpio_num_t) RESET_D);
			esp_sleep_enable_touchpad_wakeup();
			if ( deepSleepMode) {
				esp_deep_sleep_start();
			}
			esp_light_sleep_start();
			//rtc_gpio_hold_dis((gpio_num_t) RESET_D);
			cleanUpSleepDisplay();
			buttonState = 0xffff;
			//badgePadInit();
		}
	}
	vTaskDelete(NULL);
}

void badgePadInit()
{
    uint16_t touch_value;
    uint64_t t;
    touch_pad_init();
    touch_pad_set_fsm_mode(TOUCH_FSM_MODE_TIMER);
    // Set reference voltage for charging/discharging
    // For most usage scenarios, we recommend using the following combination:
    // the high reference valtage will be 2.7V - 1V = 1.7V, The low reference voltage will be 0.5V.
    touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V);
    for (int i = 0;i< TOUCH_PAD_MAX - 1;i++) {
        touch_pad_config(i, 0); // threshold
    }
    // Set thresh hold
    touch_pad_filter_start(10); // Period
    for (int i = 0; i<TOUCH_PAD_MAX - 1; i++) {
        touch_pad_read_filtered(i, &touch_value);
        touch_pad_set_thresh(i, touch_value * 2 / 3);

    }
    touch_pad_filter_stop();
    // Register touch interrupt ISR
    touch_pad_isr_register(badgePadInterrupt, NULL);

    // Start Sleep Watch Task
    t = esp_timer_get_time();
    for (int i = 0;i< TOUCH_PAD_MAX - 1;i++) {
        buttonTriggerTime[i] = t;
    }
    
    touch_pad_intr_enable();
}

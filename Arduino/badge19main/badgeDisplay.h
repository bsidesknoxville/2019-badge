/* 2019 Badge Example
   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/

#ifndef BADGEDISPLAY
#define BADGEDISPLAY
#if defined (__cplusplus)
extern "C" {
#endif
void badgeDisplayInit();
void drawSleepDisplay();
void cleanUpSleepDisplay();
#if defined (__cplusplus)
}

#include <pins_arduino.h>
#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library for ST7735
#include <SPI.h>

extern SPIClass * vspi;
extern Adafruit_ST7735 * tft;

#endif
#endif

//  Jed's GIF  example

#include <SPI.h>
#include <TFT_eSPI.h>       // Hardware-specific library, be sure to have correctly edited /Users/XXXX/Documents/Arduino/libraries/TFT_eSPI/User_Setup.h 
#include <string.h>

// The following header file is produced by extractGIFs.py
#include "giphy.h" /* Insert the Header File that defines num_frames HERE */
#include "esp_spi_flash.h"


int delayTime = 80;
TFT_eSPI tft = TFT_eSPI();  // Invoke custom library

void setup(void) {

  Serial.begin(115200);
  while(!Serial){}
  
  tft.init();
  tft.fillScreen(TFT_BLACK);
  tft.setAddrWindow(0,0,128,128);
  //tft.setSwapBytes(1);// Swap the byte order for pushImage() - corrects endianness
  
}
int x;
int y;
int offset = 0;
uint16_t data[128*128] = {0};
uint16_t *myDataPointer;
String buffer;
void loop() {

for (y = 0; y < num_frames; y++) {
  Serial.print("Printing Frame # ");
  Serial.print(y);
  Serial.print("\n");

 
  spi_flash_read(0x110000 + offset, data, sizeof(data));


  offset += 0x8000;
  
  tft.pushImage(0, 0, 128, 128, data);
  memset(data, 0, sizeof(data));

  delay (delayTime);
  if ( y == num_frames-1 ) {
    delay(250);
    offset = 0;
  }


}

/*for(x = 0; x < 40; x++){
   tft.pushImage(0,0,128,128,mario_gif+(x*128*128));
   Serial.print(x*128*128);
   delay(delayTime);
}*/


//Here is A much slower way to display an image
//for (long j = 0; j<128*128; j++){

//  tft.pushColor(dinosaur[j]);  
//}
//delay(delayTime);
//

/*
//Here is Another slow way to display an image
  int z = 0;
  for(int y = 0; y < 128; y++){
    for(int x = 0; x < 128; x++){
    tft.drawPixel(x, y, dinosaur[z]);
    z++;
    }
  }
  delay(delayTime);
  z = 0;
    for(int y = 0; y < 128; y++){
    for(int x = 0; x < 128; x++){
    tft.drawPixel(x, y, mario1[z]);
    z++;
    }
  }
  delay(delayTime);
*/

}

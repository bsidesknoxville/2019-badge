const byte interruptPin = 22;

SemaphoreHandle_t syncSemaphore;

void IRAM_ATTR handleInterrupt() {
  xSemaphoreGiveFromISR(syncSemaphore, NULL);
}

void setup() {

  Serial.begin(115200);

  syncSemaphore = xSemaphoreCreateBinary();

  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, RISING);

}

void loop() {

    xSemaphoreTake(syncSemaphore, portMAX_DELAY);
    Serial.println("Motion detected");

}

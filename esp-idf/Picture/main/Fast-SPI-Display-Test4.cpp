//  Jed's super fast display example
//  Goal:  To swap between two images as quickly as possible (so we can make a gif animation that is smooth and does not any show scan line effects)
//  We'll draw two completely different images to make it easier to discern the refresh characteristics

#include <SPI.h>
#include "Fast-SPI-Display-Test4.h"
#include <TFT_eSPI.h>       // Hardware-specific library, be sure to have correctly edited /Users/XXXX/Documents/Arduino/libraries/TFT_eSPI/User_Setup.h 
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <Arduino.h>
#include "mario_images_for_badge.h"
#include "User_Setup.h"

void app_main () {
	


int delayTime = 50;

TFT_eSPI tft = TFT_eSPI();  // Invoke custom library
  
  tft.init();
  tft.fillScreen(TFT_BLACK);
  tft.setAddrWindow(0,0,128,128);
  tft.setSwapBytes(1);// Swap the byte order for pushImage() - corrects endianness
  



tft.pushImage(0,0,128,128,mario1);
delay(delayTime);//play with delayTime to set frame rate

/*
//Here is A much slower way to display an image
for (long j = 0; j<128*128; j++){
  tft.pushColor(dinosaur[j]);  
}
delay(delayTime);
for (long j = 0; j<128*128; j++){
  tft.pushColor(mario1[j]);  
}
delay(delayTime);
*/

/*
//Here is Another slow way to display an image
  int z = 0;
  for(int y = 0; y < 128; y++){
    for(int x = 0; x < 128; x++){
    tft.drawPixel(x, y, dinosaur[z]);
    z++;
    }
  }
  delay(delayTime);
  z = 0;
    for(int y = 0; y < 128; y++){
    for(int x = 0; x < 128; x++){
    tft.drawPixel(x, y, mario1[z]);
    z++;
    }
  }
  delay(delayTime);
*/


}

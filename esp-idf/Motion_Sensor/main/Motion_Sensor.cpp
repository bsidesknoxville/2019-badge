#include <stdio.h>
#include "Motion_Sensor.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include <Arduino.h>
#include <driver/adc.h>

SemaphoreHandle_t syncSemaphore;

void IRAM_ATTR handleInterrupt() {
  xSemaphoreGiveFromISR(syncSemaphore, NULL);
}


void app_main() {

const byte interruptPin = 22;


/*
void IRAM_ATTR handleInterrupt() {
  xSemaphoreGiveFromISR(syncSemaphore, NULL);
}
*/


  Serial.begin(115200);
  syncSemaphore = xSemaphoreCreateBinary();
  pinMode(interruptPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(interruptPin), handleInterrupt, RISING);
  while (1) {

    xSemaphoreTake(syncSemaphore, portMAX_DELAY);
    Serial.println("Motion detected");
  }

}

#include <stdio.h>
#include "Sound_Sensor.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include <Arduino.h>

int sensorPin=7;
boolean val = 0;

void app_main(){
  pinMode(sensorPin, INPUT);
  Serial.begin (115200);
  while(1){
     val =digitalRead(sensorPin);
     Serial.println (val);
    // when the sensor detects a signal above the threshold value, LED flashes
    if (val==HIGH) {
     Serial.println("Sound deteected!");
     delay(200);
    }
    else {
     Serial.println("No sound detected!");
     delay(200);
    }
  }
}

#
# "main" pseudo-component makefile.
#
# (Uses default behaviour of compiling all source files in directory, adding 'include' to include path.)

COMPONENT_SRCDIRS += libraries/TFT_eSPI libraries/Adafruit_GFX_Library libraries/Adafruit_ST7735_and_ST7789_Library libraries/Adafruit_NeoPixel
COMPONENT_ADD_INCLUDEDIRS := libraries/TFT_eSPI libraries/Adafruit_GFX_Library libraries/Adafruit_ST7735_and_ST7789_Library libraries/Adafruit_NeoPixel

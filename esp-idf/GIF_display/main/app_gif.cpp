#include <SPI.h>
#include <TFT_eSPI.h>       // Hardware-specific library, be sure to have correctly edited /Users/XXXX/Documents/Arduino/libraries/TFT_eSPI/User_Setup.h 
#include <Arduino.h>
#include "gif.h" /* Insert Proper Header File HERE */
#include "esp_spi_flash.h"

extern "C" {
	void app_main();
}

uint16_t data[128*128] = {0};

void app_main () {
  Serial.begin(115200);
  Serial.println("Beginning of app");
  int delayTime = 80;
  TFT_eSPI tft = TFT_eSPI();  // Invoke custom library

  
  tft.init();
  tft.fillScreen(TFT_BLACK);
  tft.setAddrWindow(0,0,128,128);

  int y;
  int offset;

while (1) {
  offset = 0;


  for (y = 0; y < num_frames; y++) {
    Serial.println("Printing Frame # ");
    Serial.println(y);
    Serial.println("\n");

 
    spi_flash_read(0x110000 + offset, data, 32768);


    offset += 0x8000;
  
    tft.pushImage(0, 0, 128, 128, data);

    delay (delayTime);
    if ( y == num_frames-1 ) {
      delay(250);
      offset = 0;
    }

  }
}
}

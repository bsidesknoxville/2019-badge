## Tutorial for a Picture with Espressif Command Line
**Ensure that your board is set-up properly for Espressif and that you have successfully run the hello world and 2019 badge programs**: [Click Here For Set-up Tutorial](https://gitlab.com/bsidesknoxville/2019-badge/tree/master)
1. Change Directory to the 2019-badge/esp-idf/Picture directory in your terminal.
2. On your command line, run the command -> `make`
3. Once the make command is complete hook up your badge with the correct cabling and flash/program your board -> `make flash` 
    - When you see this ` Connecting........_____...`
     -  Press down the S1 button...while holding the S1 button down press the S9 button, release the S9 button,then release the S1 button
    - When you see this `Leaving... Hard resetting via RTS pin...`
         -  Press the S9 button and you should see the menu display on the badge
4. The mario picture should be showing on your screen!
5. If you want to do your own picture now....follow these steps
    - First thing to do is pick a picture you would like to display on your 128x128 screen
    - If your picture is not 128 pixels by 128 pixels as most are not, you will need to resize the picture down to that size using GIMP or some other picture resizing tool such as…
        - If you are using GIMP, you need to download it from https://www.gimp.org/downloads/ and then use this tutorial to scale the image -> https://guides.lib.umich.edu/c.php?g=282942&p=1888162
        - https://resizeimage.net/
        - https://www.imgonline.com.ua/eng/resize-image.php
    - Next, your picture needs to be in a hexadecimal C array in order to save it to the flash. You must have 16 bit hex colors….meaning it must look like this -> 0xffff (4-digits after the ‘0x’) 
        - An example picture that is in a C array format is in the mario_images_for_badge.h file on the repo 
         ```
        #ifndef MARIO_IMAGES_FOR_BADGE_H
        #define MARIO_IMAGES_FOR_BADGE_H
        const uint16_t mario1[] PROGMEM={
        0x5C7A, 0x651F, 0x651F, 0x64FF, 0x64FF, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F,   // 0x0010 (16) pixels
        0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F,   // 0x0020 (32) pixels
        0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F,   // 0x0030 (48) pixels
        .... 
            
        };
        #endif
    
        ```
    
        - Use this link to convert your resized picture file to a C array: http://www.rinkydinkelectronics.com/t_imageconverter565.php
    - Once your picture is in a C array format, you need to create a header file that is similar in format to the mario_images_for_badge.h file with the PROG_MEM and #Ifdef_ statements. 
    - Take your C array values and put it inside the brackets -> { }; 
    - Now use the Fast-SPI-Display-Test4.cpp file to run your picture on the badge. 
    - In the Fast-SPI-Display-Test4.cpp, you will see #include "mario_images_for_badge.h", use this format with the name of your pictures header file. 
    ```
    #include <SPI.h>
    #include <TFT_eSPI.h>       // Hardware-specific library, be sure to have correctly edited /Users/XXXX/Documents/Arduino/libraries/TFT_eSPI/User_Setup.h 
    #include "mario_images_for_badge.h"
    ```
    - Also you will see a line like this -> tft.pushImage(0,0,128,128,mario1); (instead of mario1, put the name of your picture array there)
    ```
    void loop() {
    tft.pushImage(0,0,128,128,mario1);
    delay(delayTime);//play with delayTime to set frame rate
    }
    ```
    - Once this is complete, follow steps 2-3 above

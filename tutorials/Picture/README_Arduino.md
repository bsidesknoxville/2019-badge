## Putting a Picture on Your Screen: 
**Ensure that your board is set-up properly**: [Click Here For Set-up Tutorial](https://gitlab.com/bsidesknoxville/2019-badge/tree/master)
1. First thing to do is pick a picture you would like to display on your 128x128 screen
2. If your picture is not 128 pixels by 128 pixels as most are not, you will need to resize the picture down to that size using GIMP or some other picture resizing tool such as…
    - If you are using GIMP, you need to download it from https://www.gimp.org/downloads/ and then use this tutorial to scale the image -> https://guides.lib.umich.edu/c.php?g=282942&p=1888162
    - https://resizeimage.net/
    - https://www.imgonline.com.ua/eng/resize-image.php
3. Next, your picture needs to be in a hexadecimal C array in order to save it to the flash. You must have 16 bit hex colors….meaning it must look like this -> 0xffff (4-digits after the ‘0x’) 
    - An example picture that is in a C array format is in the mario_images_for_badge.h file on the repo 
    ```
        #ifndef MARIO_IMAGES_FOR_BADGE_H
        #define MARIO_IMAGES_FOR_BADGE_H
        const uint16_t mario1[] PROGMEM={
        0x5C7A, 0x651F, 0x651F, 0x64FF, 0x64FF, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F,   // 0x0010 (16) pixels
        0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F,   // 0x0020 (32) pixels
        0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F, 0x651F,   // 0x0030 (48) pixels
        .... 
            
        };
        #endif

    ```

    - Use this link to convert your resized picture file to a C array: http://www.rinkydinkelectronics.com/t_imageconverter565.php
4. Once your picture is in a C array format, you need to create a header file that is similar in format to the mario_images_for_badge.h file with the PROG_MEM and #Ifdef_ statements. 
    - Take your C array values and put it inside the brackets -> { }; 
5. Now open the Picture Sketchbook in Arduino to run your picture on the badge. 
    - In Picture.ino, you will see #include "mario_images_for_badge.h", use this format with the name of your pictures header file. 
    ```
    #include <SPI.h>
    #include <TFT_eSPI.h>       // Hardware-specific library, be sure to have correctly edited /Users/XXXX/Documents/Arduino/libraries/TFT_eSPI/User_Setup.h 
    #include "mario_images_for_badge.h"
    ```
    - Also you will see a line like this -> tft.pushImage(0,0,128,128,mario1); (instead of mario1, put the name of your picture array there)
    ```
    void loop() {
    tft.pushImage(0,0,128,128,mario1);
    delay(delayTime);//play with delayTime to set frame rate
    }
    ```

6. Last step is to run your code on the Arduino IDE 
    - Compile your code
    - Upload your code 
        - When you see this on your screen 
        ```
        esptool.py v2.6-beta1
        Serial port /dev/cu.SLAB_USBtoUART
        Connecting........_____...
        ```
        - Press down the S1 button...while holding the S1 button down press the S9 button, release the S9 button,then release the S1 button
        
        - You will then see this on your screen 
        ```
        Chip is ESP32D0WDQ5 (revision 1)
        Features: WiFi, BT, Dual Core, 240MHz, VRef calibration in efuse, Coding Scheme None
        MAC: b4:e6:2d:d4:22:49
        Uploading stub...
        Running stub...
        Stub running...
        Configuring flash size...
        Auto-detected Flash size: 16MB
        Compressed 8192 bytes to 47...

        Writing at 0x0000e000... (100 %)
        Wrote 8192 bytes (47 compressed) at 0x0000e000 in 0.0 seconds (effective 7389.4 kbit/s)...
        Hash of data verified.
        Flash params set to 0x024f
        Compressed 17664 bytes to 11528...

        Writing at 0x00001000... (100 %)
        Wrote 17664 bytes (11528 compressed) at 0x00001000 in 1.0 seconds (effective 138.7 kbit/s)...
        Hash of data verified.
        Compressed 296208 bytes to 133929...

        Writing at 0x00010000... (11 %)
        Writing at 0x00014000... (22 %)
        Writing at 0x00018000... (33 %)
        Writing at 0x0001c000... (44 %)
        Writing at 0x00020000... (55 %)
        Writing at 0x00024000... (66 %)
        Writing at 0x00028000... (77 %)
        Writing at 0x0002c000... (88 %)
        Writing at 0x00030000... (100 %)
        Wrote 296208 bytes (133929 compressed) at 0x00010000 in 11.8 seconds (effective 200.3 kbit/s)...
        Hash of data verified.
        Compressed 3072 bytes to 144...

        Writing at 0x00008000... (100 %)
        Wrote 3072 bytes (144 compressed) at 0x00008000 in 0.0 seconds (effective 1374.7 kbit/s)...
        Hash of data verified.

        Leaving...
        Hard resetting via RTS pin...
        ```
        - When you see "Hard resetting via RTS pin", press S9 button as this is your hard RTS pin 
    
7. Your picture should be showing on the screen on your badge
*Another good reference for loading pictures on board is at this link -> https://www.instructables.com/id/Converting-Images-to-Flash-Memory-Iconsimages-for-/

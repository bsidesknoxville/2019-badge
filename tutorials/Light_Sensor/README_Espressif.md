## Light Sensor Tutorial for Espressif 
- **Ensure that your board is set-up properly**: [Click Here For Set-up Tutorial](https://gitlab.com/bsidesknoxville/2019-badge/tree/master)

1. Grab a soldering iron and lets SOLDER! Use three wires to connect your sensor  to PWR, GND, and TP32 on your board.(Your TP pin must be I/0 and work as ADC **Check WROVER-B Datasheet to double check your pins)**

![0](0.JPG)
![1](1.JPG)

2. After ensuring your wires are connected to properly, it's time to upload our software.
3. Change your directory to 2019-badge/esp-idf/Light_Sensor in your terminal
4. On your command line, run the command -> `make`
5. Once the make command is complete hook up your badge with the correct cabling and flash/program your board -> `make flash` 
    - When you see this ` Connecting........_____...`
     -  Press down the S1 button...while holding the S1 button down press the S9 button, release the S9 button,then release the S1 button
    - When you see this `Leaving... Hard resetting via RTS pin...`
        - On your command line, run the command -> `make monitor`
        - Then press S9 for the hard reset
6. You should be seeing different values, so if you cover up the photoresistor the numbers should change, shine a flashlight on it and you'll see the numbers change

## Putting a GIF on Your Screen with Espressif  

**Ensure that your board is set-up properly**: [Click Here For Set-up Tutorial](https://gitlab.com/bsidesknoxville/2019-badge/tree/master)
1. First thing to do is pick a GIF you would like to display on your 128x128 screen. Store this GIF in a folder on your desktop. **The GIF file size should be under 16 Mb due to the flash memory size on the chip**
2. Next thing to do is convert your GIF's frames and pixels to a header and binary file using the extactedGif.py program in this folder
    - To run this program type: python extractedGif.py  name_of_gif_file  name_of_directory  name_of_header_file  name_of_binary_file
3. Once you have your header and binary file, add your header file to the 2019-badge/esp-idf/GIF_display/main folder. 
4. The binary file has to be put into flash memory. In order to do this run these commands: 
    -  sudo pip install esptool
    -  esptool.py --port /dev/ttyUSB0 write_flash 0x110000 name_of_directory/name_of_binary_file 
        - When you see this ` Connecting........_____...`
            -  Press down the S1 button...while holding the S1 button down press the S9 button, release the S9 button,then release the S1 button
        - When you see this `Leaving... Hard resetting via RTS pin...`
            -  Press the S9 button and you should see the menu display on the badge
5. To run the code, compile the code and then upload the code
    - Make sure that you include the proper header file in the app_gif.cpp file, this will allow the system to find where your GIF is located **Make sure this GIF header file is in the main directory!**
        ```
         #include "name_of_header_file.h"
        ```
    -  Add our default.csv file from this folder to your 2019-badge/esp-idf/GIF_display folder 
6. On your command line, run the command -> `make`
7. Once the make command is complete hook up your badge with the correct cabling and flash/program your board -> `make flash` 
    - When you see this ` Connecting........_____...`
     -  Press down the S1 button...while holding the S1 button down press the S9 button, release the S9 button,then release the S1 button
    - When you see this `Leaving... Hard resetting via RTS pin...`
         -  Press the S9 button and you should see the menu display on the badge
    
8. Your GIF should be displayed on the screen!

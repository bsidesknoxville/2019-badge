## Sound Sensor Tutorial for Espressif 
- **Ensure that your board is set-up properly**: [Click Here For Set-up Tutorial](https://gitlab.com/bsidesknoxville/2019-badge/tree/master)

1. Grab a soldering iron and lets SOLDER! Use three wires to connect your sensor  to PWR, GND, and TP22(or any open I/O pin **Check schematic or datasheet on your board.**

![0](0.JPG)
![1](1.JPG)
![2](2.JPG)

2. After ensuring your wires are connected to properly, it's time to upload our software.
3. Change directory in your terminal to the 2019-badge/esp-idf/Sound_Sensor directory.
4. On your command line, run the command -> `make`
5. Once the make command is complete hook up your badge with the correct cabling and flash/program your board -> `make flash` 
    - When you see this ` Connecting........_____...`
     -  Press down the S1 button...while holding the S1 button down press the S9 button, release the S9 button,then release the S1 button
    - When you see this `Leaving... Hard resetting via RTS pin...`
        - On your command line, run the command -> `make monitor`
        - Then press S9 for the hard reset
6. Clap in front of the sensor and see what it says on the monitor!

## Sound Sensor Tutorial
- **Ensure that your board is set-up properly**: [Click Here For Set-up Tutorial](https://gitlab.com/bsidesknoxville/2019-badge/tree/master)

1. Grab a soldering iron and lets SOLDER! Use three wires to connect your sensor  to PWR, GND, and TP22(or any open I/O pin **Check schematic or datasheet) on your board.**

![0](0.JPG)
![1](1.JPG)
![2](2.JPG)

2. After ensuring your wires are connected to properly, it's time to upload our software. Download the Sound_Sensor Sketchbook in Arduino IDE.

3. To run the code, compile the code and then upload the code
 
4. Last step is to run your code on the Arduino IDE 
    - When you see this on your screen 
        ```
        esptool.py v2.6-beta1
        Serial port /dev/ttyUSB0
        Connecting........_____...
        ```
    - Press down the S1 button...while holding the S1 button down press the S9 button, release the S9 button,then release the S1 button
        
    - You will then see this on your screen 
        ```
        Chip is ESP32D0WDQ5 (revision 1)
        Features: WiFi, BT, Dual Core, 240MHz, VRef calibration in efuse, Coding Scheme None
        MAC: b4:e6:2d:d4:22:49
        Uploading stub...
        Running stub...
        Stub running...
        Configuring flash size...
        Auto-detected Flash size: 16MB
        Compressed 8192 bytes to 47...

        Writing at 0x0000e000... (100 %)
        Wrote 8192 bytes (47 compressed) at 0x0000e000 in 0.0 seconds (effective 7389.4 kbit/s)...
        Hash of data verified.
        Flash params set to 0x024f
        Compressed 17664 bytes to 11528...

        Writing at 0x00001000... (100 %)
        Wrote 17664 bytes (11528 compressed) at 0x00001000 in 1.0 seconds (effective 138.7 kbit/s)...
        Hash of data verified.
        Compressed 296208 bytes to 133929...

        Writing at 0x00010000... (11 %)
        Writing at 0x00014000... (22 %)
        Writing at 0x00018000... (33 %)
        Writing at 0x0001c000... (44 %)
        Writing at 0x00020000... (55 %)
        Writing at 0x00024000... (66 %)
        Writing at 0x00028000... (77 %)
        Writing at 0x0002c000... (88 %)
        Writing at 0x00030000... (100 %)
        Wrote 296208 bytes (133929 compressed) at 0x00010000 in 11.8 seconds (effective 200.3 kbit/s)...
        Hash of data verified.
        Compressed 3072 bytes to 144...

        Writing at 0x00008000... (100 %)
        Wrote 3072 bytes (144 compressed) at 0x00008000 in 0.0 seconds (effective 1374.7 kbit/s)...
        Hash of data verified.

        Leaving...
        Hard resetting via RTS pin...
        ```
    - When you see "Hard resetting via RTS pin", press S9 button as this is your hard RTS pin 
    
8. Open your serial monitor and see what it says. 
    
    

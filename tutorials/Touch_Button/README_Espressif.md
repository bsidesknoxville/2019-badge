## Seeing What Touch Button You Are Pressing On Screen 
This tutorial is for you to see a sample on how to make an app and also how to use the interrupt functions for the capacitive touch buttons. 
1. **Ensure that your board is set-up properly**: [Click Here For Set-up Tutorial](https://gitlab.com/bsidesknoxville/2019-badge/tree/master)
2. Copy the app_sample.cpp in this directory and replace the app_sample.cpp in the Arduino/badge19main/main directory
2. Make 2019 Badge Code `cd ~/esp/2019-badge/esp-idf/badge19main; make`
3. Flash/Program with `make flash`
 - When you see this ` Connecting........_____...`
     -  Press down the S1 button...while holding the S1 button down press the S9 button, release the S9 button,then release the S1 button
 - When you see this `Leaving... Hard resetting via RTS pin...`
     -  Press the S9 button and you should see the menu display on the badge
4. Using the capacitive touch buttons scroll (UP = S2, DOWN = S3) down the menu list to the "SAMPLE" app and press S7 (A)
5. Now touch the capacitive touch buttons for the app to work. 

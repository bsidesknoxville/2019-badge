## LED Ring Tutorial for Espressif 
This tutorial is for you to see a sample on how to solder an LED Ring to your badge and make it run on the Espressif environment
1. **Ensure that your board is set-up properly**: [Click Here For Set-up Tutorial](https://gitlab.com/bsidesknoxville/2019-badge/tree/master)

2. Grab a soldering iron and lets SOLDER! Use three wires to connect your LED ring to PWR, GND, and TP21 on your board.

![0](0.JPG) 
![1](1.JPG) 

3. After ensuring your wires are connected to properly, it's time to upload our software.
4. Change Directory to 2019-badge/esp-idf/LED_Pixel_Ring
5. On your command line, run the command -> `make`
6. Once the make command is complete hook up your badge with the correct cabling and flash/program your board -> `make flash` 
    - When you see this ` Connecting........_____...`
     -  Press down the S1 button...while holding the S1 button down press the S9 button, release the S9 button,then release the S1 button
    - When you see this `Leaving... Hard resetting via RTS pin...`
         -  Press the S9 button and you should see the menu display on the badge
7. The LED ring should be lighting up!

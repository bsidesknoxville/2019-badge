# Getting started

## Download this repository and extract it somewhere
* [Download Link](https://gitlab.com/bsidesknoxville/2019-badge/-/archive/master/2019-badge-master.zip)
* ```cd ~; mkdir -p esp; cd esp; unzip ~/Downloads/2019-badge-master.zip; mv 2019-badge-master 2019-badge```
* or clone this repo ```cd ~; mkdir -p esp; cd esp; git clone https://gitlab.com/bsidesknoxville/2019-badge```

## Install Arduino IDE
  - Download the Arduino Software from [http://www.arduino.cc/en/Main/Software](http://www.arduino.cc/en/Main/Software)
    - Choose the ARM32 install for Raspberry Pi.

### ESP32 Compiler Setup
  - For Linux:

```
cd 2019-badge/Arduino/hardware/espressif/esp32/tools
python3 get.py
cd -
```

  - For Raspberry Pi 3 (ARM):

```
cd 2019-badge/Arduino/hardware/espressif/esp32/tools
python3 get.py
tar xzvf xtensa-esp32-elf-arm.tar.gz
cd -
```
  - For Windows (Not tested):
    - Open `2019-badge/Arduino/hardware/espressif/esp32/tools/`
    - Run `Get.exe`

### Change Sketchbook Location
  - Open Preferences in the Arduino editor
  - Change Sketchbook location to the Arduino folder location here.
  - Restart your IDE

### Open up a sketchbook
  - Open the sketchbook of your choice
    - File -> Sketchbook -> badge19main  (for example)

### Choose the 2019 Badge Board
  - Tools -> Board: -> "BSides Badge 2019"
  - Tools -> Upload Speed: -> "115200"
  - Tools -> Flash Size: -> "16 MB (128 Mb)"
  - Tools -> Partition Scheme: -> "7M APPS with OTA"
  - Make sure to select the right port as well: Tools -> Port

## Install ESP-IDF

  * Arduino will usually recompile sources that were unchanged resulting in long compile times. In order to have more control over compilation, we recommended installing the esp-idf.

### Download ESP-IDF and all submodules and requirements

  * References:
    * https://docs.espressif.com/projects/esp-idf/en/stable/get-started/
    * https://docs.espressif.com/projects/esp-idf/en/stable/get-started/linux-setup.html

```
# First, follow instructions for installing ESP32 compiler above.
cd ~
mkdir -p ~/esp
cd ~/esp
git clone -b v3.2 --recursive https://github.com/espressif/esp-idf.git
cat << 'EOF' >> $HOME/.profile
export PATH="$PATH:$HOME/esp/2019-badge/Arduino/hardware/espressif/esp32/tools/xtensa-esp32-elf/bin"
export IDF_PATH="$HOME/esp/esp-idf"
EOF
export PATH="$PATH:$HOME/esp/2019-badge/Arduino/hardware/espressif/esp32/tools/xtensa-esp32-elf/bin"
export IDF_PATH="$HOME/esp/esp-idf"
sudo apt -y update
sudo apt -y upgrade
sudo apt -y install gcc git wget make libncurses-dev flex bison gperf python python-pip python-setuptools python-serial python-cryptography python-future libffi-dev libssl-dev
python -m pip install --user -r $IDF_PATH/requirements.txt
```

### Copy Example and Compile, etc.

* Copy ```cp -r $IDF_PATH/examples/get-started/hello_world ~/esp/```
* Make ```cd ~/esp/hello_world; make```
  * Make 2019 Badge Code ```cd ~/esp/2019-badge/esp-idf/badge19main; make```
* Flash ```make flash```
* Configure ```make menuconfig```
* Serial Monitor ```make monitor```

